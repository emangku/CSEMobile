package com.creativeit.csemobile.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

@SuppressWarnings("deprecation")
public class RegionFragment extends Fragment {
    View view;
    @BindView(R.id.viewSelectSpinner)
    Spinner viewSelectSpinner;
    @BindView(R.id.viewSelectBranch)
    Button viewSelectBranch;
    String opt = "";
    String reg = "";
    String regions = "";
    private List<String> list;
    ArrayAdapter<String> adapter;
    String[] region = {"--Select Region--", "JAK", "BALI", "BANDUNG"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.region_frgment, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    void init() {
        loadSpinerIdType();
    }

    void loadSpinerIdType() {
        loadRegion();
    }

    @OnClick(R.id.viewSelectBranch)
    void viewSelectBranch() {
        if (opt.equals("")) {
            Toast.makeText(view.getContext(), "Silahkan Pilih Region", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(view.getContext(), opt, Toast.LENGTH_LONG).show();

            if (opt.equals("BALI")) {
                reg = "BAL";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("JAKARTA")) {
                reg = "JAK";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("BANDUNG")) {
                reg = "BDG";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("SURABAYA")) {
                reg = "SUB";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("MAKASAR")) {
                reg = "MKS";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("BATAM")) {
                reg = "BAT";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("YOGYAKARTA")) {
                reg = "YOG";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("MEDAN")) {
                reg = "MDN";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            if (opt.equals("HEAD OFFICE")) {
                reg = "HO";
                utils.setDataSession(view.getContext(), "sessionvRegion", opt);
                utils.setDataSession(view.getContext(), "sessionvRegCode", reg);
            }
            ((FragmentActivity)view.getContext())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.frame, new StoreFragment()).commit();
        }
    }

    void loadRegion() {
        final ProgressDialog dialog = utils.showDialogBar(view.getContext(), "Loading...");
        dialog.show();
        AndroidNetworking.get(getResources().getString(R.string.url_region))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                list = new ArrayList<String>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    regions = object.getString("REG_CODE");
                                    list.add(regions);
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        adapter = new ArrayAdapter<String>(view.getContext(), R.layout.support_simple_spinner_dropdown_item, list);
                        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        viewSelectSpinner.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {

                        dialog.dismiss();
                    }
                });
    }

    @OnItemSelected(R.id.viewSelectSpinner)
    void viewSelectSpinner(int position) {
        opt = viewSelectSpinner.getItemAtPosition(position).toString();
    }

}
