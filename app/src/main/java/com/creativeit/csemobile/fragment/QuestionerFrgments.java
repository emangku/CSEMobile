package com.creativeit.csemobile.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.activity.QuestionerActivity;
import com.creativeit.csemobile.helpers.utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class QuestionerFrgments extends Fragment {
    View view;
    @BindView(R.id.viewChangeStore)Button viewChangeStore;
    @BindView(R.id.viewQuestionerSurveyHistory)Button viewQuestionerSurveyHistory;
    @BindView(R.id.viewQuestionerBtnSheet)Button viewQuestionerBtnSheet;
    @BindView(R.id.viewHeaderTitle)TextView viewHeaderTitle;
    String session_store_branch;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.questioner_frgment, container,false);
        ButterKnife.bind(this,view);
        session_store_branch =  utils.getDataSession(view.getContext(),"sessionStore");
        viewHeaderTitle.setText(utils.getDataSession(view.getContext(),"sessionvRegion")+" - "+session_store_branch);
        return view;
    }

    @OnClick(R.id.viewQuestionerBtnSheet) void viewQuestionerBtnSheet(){
        startActivity(new Intent(view.getContext(), QuestionerActivity.class));
    }
    @OnClick(R.id.viewQuestionerSurveyHistory) void viewQuestionerSurveyHistory(){

    }
    @OnClick(R.id.viewChangeStore)void viewChangeStore(){

    }

}
