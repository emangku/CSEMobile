package com.creativeit.csemobile.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.activity.MainMenuActivity;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

/**
 * Created by Creef on 5/3/2018.
 */

@SuppressWarnings("deprecation")
public class StoreFragment extends Fragment {
    private View view;
    @BindView(R.id.viewSelectedStore)Spinner viewSelectedStore;
    @BindView(R.id.viewBTNStoreSelected)Button viewBTNStoreSelected;
    @BindView(R.id.viewBackChangeBranch)Button viewBackChangeBranch;
    @BindView(R.id.viewSessRegion)TextView viewSessRegion;
    String opt = "";
    String store = "";
    ArrayAdapter<String> adapter;
    List<String> list;

    String sessionReg = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.store_frgment, container, false);
        ButterKnife.bind(this, view);
        viewSessRegion.setText(utils.getDataSession(view.getContext(),"sessionvRegion"));
        init();
        return view;
    }

    void init(){
        sessionReg = utils.getDataSession(view.getContext(),"sessionvRegCode");
        LoadStoreByregion();
    }

    void LoadStoreByregion(){
        final ProgressDialog dialog = utils.showDialogBar(view.getContext(),"Loading...");
        dialog.show();
        AndroidNetworking.post(getResources().getString(R.string.url_store_dest))
                .addBodyParameter("REG_CODE",sessionReg)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                list = new ArrayList<String>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    store = object.getString("BU_CODE") + " - " +object.getString("BU_NAME") ;
                                    list.add(store);
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        adapter = new ArrayAdapter<String>(view.getContext(),R.layout.support_simple_spinner_dropdown_item,list);
                        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        viewSelectedStore.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(),anError.getMessage(),Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }

    @OnItemSelected(R.id.viewSelectedStore) void viewSelectedStore(int position){
        opt = viewSelectedStore.getItemAtPosition(position).toString();
        utils.setDataSession(view.getContext(),"sessionStore",opt);
    }

    @OnClick(R.id.viewBTNStoreSelected) void viewBTNStoreSelected(){

        if (opt.equals("")){
            Toast.makeText(view.getContext(),"Silahkan Pilih", Toast.LENGTH_LONG).show();
        }else{
            startActivity(new Intent(view.getContext(), MainMenuActivity.class));
        }

    }
    @OnClick(R.id.viewBackChangeBranch) void viewBackChangeBranch(){
        ((FragmentActivity)view.getContext()).getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("Region Branch")
                .replace(R.id.frame, new RegionFragment())
                .commit();
        utils.removeSessionData(view.getContext(),"sessionvRegCode");
    }
}
