package com.creativeit.csemobile.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.activity.HomeActivity;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Creef on 5/3/2018.
 */

@SuppressWarnings("deprecation")
public class LoginFrgments extends Fragment {
    private View view;

    @BindView(R.id.viewHRISLoginButton) Button viewHRISLoginButton;
    @BindView(R.id.viewHRISResetForm) Button viewHRISResetForm;
    @BindView(R.id.viewHRISPassword) EditText viewHRISPassword;
    @BindView(R.id.viewHRISAccount) EditText viewHRISAccount;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_fragment, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick(R.id.viewHRISLoginButton)void viewHRISLoginButton(){
        LoginuserHRisAurh();
    }
    @OnClick(R.id.viewHRISResetForm)void viewHRISResetForm(){
        resetform();
    }

    void resetform(){
        viewHRISAccount.setText("");
        viewHRISPassword.setText("");
    }

    void LoginuserHRisAurh(){
        final ProgressDialog dialog = utils.showDialogBar(view.getContext(), getResources().getString(R.string.loading_bar_msg));
        dialog.show();
        AndroidNetworking.post(getResources().getString(R.string.login_session))
                .addBodyParameter("USRMS_ID", viewHRISAccount.getText().toString())
                .addBodyParameter("U_LOGIN_PS",viewHRISPassword.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                           if (response.getString("status").equals("true")){
                               if (!response.getJSONObject("data").getString("USRMS_ID").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionUSRMS_ID",response.getJSONObject("data").getString("USRMS_ID"));
                               }
                               if (!response.getJSONObject("data").getString("USRMS_NAME").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionUSRMS_NAME",response.getJSONObject("data").getString("USRMS_NAME"));
                               }
                               if (!response.getJSONObject("data").getString("HR_CODE").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionHR_CODE",response.getJSONObject("data").getString("HR_CODE"));
                               }
                               if (!response.getJSONObject("data").getString("USERID_CODE").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionUSERID_CODE",response.getJSONObject("data").getString("USERID_CODE"));
                               }
                               if (!response.getJSONObject("data").getString("LOGIN_ID").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionLOGIN_ID",response.getJSONObject("data").getString("LOGIN_ID"));
                               }
                               if (!response.getJSONObject("data").getString("U_LOGIN_PS").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionU_LOGIN_PS",response.getJSONObject("data").getString("U_LOGIN_PS"));
                               }
                               if (!response.getJSONObject("data").getString("REG_CODE").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionREG_CODE",response.getJSONObject("data").getString("REG_CODE"));
                               }
                               if (!response.getJSONObject("data").getString("REG_CODE").equals("null")){
                                   utils.setDataSession(view.getContext(),"sessionREG_CODE",response.getJSONObject("data").getString("REG_CODE"));
                               }

                               ((FragmentActivity)view.getContext())
                                       .getSupportFragmentManager()
                                       .beginTransaction()
                                       .addToBackStack(null)
                                       .replace(R.id.frame, new RegionFragment()).commit();
                               dialog.dismiss();
                               HomeActivity.getSviewUserIdCode();
                               HomeActivity.getSviewUserName();
                           }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(), getResources().getString(R.string.msg_error) + anError.getMessage(), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }
}
