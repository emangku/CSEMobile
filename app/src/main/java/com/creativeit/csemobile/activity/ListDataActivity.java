package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.adapter.sync.QSTAdapter;
import com.creativeit.csemobile.helpers.network_check_connection.NetworkStateChecker;
import com.creativeit.csemobile.helpers.sql_db.DatabaseHelper;
import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;
import com.creativeit.csemobile.helpers.utils;
import com.creativeit.csemobile.model.QuestionerModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("deprecation")
public class ListDataActivity extends AppCompatActivity {

    public static final String NAME_SYNCED_WITH_SERVER = "1";
    public static final String NAME_NOT_SYNCED_WITH_SERVER = "0";
    public static final String DATA_SAVED_BROADCAST = "com.creativeit.csemobile.activity";
    private static int score = 0;
    public boolean status = false;
    DatabaseHelper db = new DatabaseHelper(this);
    ListView viewList;
    String[] corrAns = new String[6];
    Cursor c1;
    Cursor c2;
    Cursor c3;
    int counter = 1;
    String label;
    RadioGroup radiogroup;
    Button btnNext, btnSave;
    private List<QuestionerModel> list;
    private BroadcastReceiver broadcastReceiver;
    private QSTAdapter adapter;
    private RadioButton radioButton, btn;
    private TextView quizQuestion;
    private TextView tvScore;
    private int rowIndex = 1;
    private int questNo = 0;
    private boolean checked = false;
    private boolean flag = true;
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_data);

        String options[] = new String[23];
//        radiogroup = (RadioGroup) findViewById(R.id.rdbGp1);

        LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.WRAP_CONTENT,
                RadioGroup.LayoutParams.WRAP_CONTENT);

        try {
            db.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        c3 = db.getCorrAns();

//        tvScore = (TextView) findViewById(R.id.tvScore);

        for (int i = 0; i <= 5; i++) {
            corrAns[i] = c3.getString(0);
            c3.moveToNext();

        }

//        radioGroup = (RadioGroup) findViewById(R.id.rdbGp1);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                for (int i = 0; i < 4; i++) {
                    btn = (RadioButton) radioGroup.getChildAt(i);
                    String text;
                    if (btn.isPressed() && btn.isChecked() && questNo < 6) {

                        Log.e("corrAns[questNo]", corrAns[questNo]);

                        if (corrAns[questNo].equals(btn.getText()) && flag == true) {
                            flag = false;
                            checked = true;
                            status = true;
                        } else if (checked == true) {
                            flag = true;
                            checked = false;
                            status = false;
                        }
                    }

                    tvScore.setText("Score: " + Integer.toString(score) + "/6");
                    Log.e("Score:", Integer.toString(score));
                }
            }
        });

//        quizQuestion = (TextView) findViewById(R.id.TextView01);
        displayQuestion();
//        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(btnNext_Listener);
//        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(btnSave_Listener);
    }

    private View.OnClickListener btnNext_Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            flag=true;
            checked = false;
            questNo++;

            if (questNo < 6){
                c1.moveToNext();
                displayQuestion();
                if (status){
                    score++;
                    status=false;
                }
                else{
                    score--;
                    status=false;
                }
            }

            else {
                tvScore.setVisibility(v.VISIBLE);
                tvScore.setText("Score: " + Integer.toString(score) + "/6");
                Toast.makeText(getApplicationContext(),"Scrore is: " + Integer.toString(score) + "/6", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(),"No More Questions",Toast.LENGTH_LONG).show();
                score=0;
            }
        }
    };
    private View.OnClickListener btnSave_Listener= new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (status){
                Toast.makeText(getApplicationContext(),"Correct Answer!!!",Toast.LENGTH_LONG).show();
                status=false;
            }
            else{
                Toast.makeText(getApplicationContext(),"Wrong!! Correct Answer is: " + corrAns[questNo],Toast.LENGTH_LONG).show();
                status=false;
            }
        }
    };

    private void displayQuestion(){
        c1=db.getQuiz_Content(rowIndex);
        c2 =db.getAns(rowIndex++);
        quizQuestion.setText(c1.getString(0));
        radioGroup.removeAllViews();
        for (int i=0;i<=3;i++){
            radioButton = new RadioButton(this);
            radioButton.setText(c2.getString(0));
            radioButton.setId(i);
            c2.moveToNext();
            radioGroup.addView(radioButton);
        }
    }
    void init() {
        viewList = (ListView) findViewById(R.id.viewList);
        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        db = new DatabaseHelper(this);
        list = new ArrayList<>();
        loadQuestioner();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadQuestioner();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(DATA_SAVED_BROADCAST));
    }

    private void loadQuestioner() {
        list.clear();
        Cursor cursor = db.getQuestioner();
        if (cursor.moveToFirst()) {
            do {
                QuestionerModel questioner = new QuestionerModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS))
                );
                list.add(questioner);
            } while (cursor.moveToNext());
        }
        adapter = new QSTAdapter(this, R.layout.questioner_list, list);
        viewList.setAdapter(adapter);
    }

    void onrefresh() {
        adapter.notifyDataSetChanged();
    }


    void saveToServer() {
        ProgressDialog dialog = utils.showDialogBar(this, "Loading");
        dialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };
    }
}
