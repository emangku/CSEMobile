package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.gps_location.GPSTracking;
import com.creativeit.csemobile.helpers.api.WebServiceHelper;
import com.creativeit.csemobile.helpers.api.vApi;
import com.creativeit.csemobile.helpers.ssl.SSLVSoket;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import okhttp3.FormBody;
import okhttp3.RequestBody;

@SuppressWarnings({"deprecation", "RedundantStringToString"})
public class MainActivity extends AppCompatActivity {
    @BindView(R.id.viewHRISAccount)EditText viewHRISAccount;
    @BindView(R.id.viewHRISPassword)EditText viewHRISPassword;
    @BindView(R.id.viewHRISLoginButton)Button viewHRISLoginButton;
    @BindView(R.id.viewHRISResetForm)Button viewHRISResetForm;
    double latitude;
    double longitude;
    GPSTracking gps;
    private Socket client;
    private PrintWriter printwriter;
    private EditText textField;
    private Button button;
    private String messsage;
    private String ipAddress = "";
    Socket socket;
    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (!WebServiceHelper.isNetworkAvailable(MainActivity.this)){
            Toast.makeText(getApplicationContext(),"Not Connection Networking : "+ipAddress, Toast.LENGTH_LONG).show();
        }else {
            try {
                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                    NetworkInterface intf = en.nextElement();
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            ipAddress = inetAddress.getHostAddress().toString();
                            Log.i("Here is the Address",ipAddress);
                        }
                    }
                }
            } catch (SocketException ex) {
                ex.fillInStackTrace();
            }

            Toast.makeText(getApplicationContext(),"Connection Networking : "+ipAddress, Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.viewHRISLoginButton)
    void viewHRISLoginButton() {
        LoginuserHRisAuth();
//       new LoginHRIS().execute((Void)null);
//        inits();
    }

    @OnClick(R.id.viewHRISResetForm)
    void viewHRISResetForm() {
        viewHRISAccount.setText("");
        viewHRISPassword.setText("");
    }

    void getGPS(){
        gps = new GPSTracking(this);
        if (gps.canGetLocation()){
            latitude = gps.getLatitude();
            longitude= gps.getLongitude();
            Log.d("Location : ",gps.formatLatLongGPSTRACKING(latitude,longitude));
        }else{
            gps.showSettingAlerts();
        }
    }
    void LoginuserHRisAuth() {
        AndroidNetworking.post(getResources().getString(R.string.login))
                .setOkHttpClient(SSLVSoket.getUnsafeOkHttpClient().build())
                .addBodyParameter("USRMS_ID", viewHRISAccount.getText().toString())
                .addBodyParameter("U_LOGIN_PS", viewHRISPassword.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                if (!response.getJSONObject("data").getString("USRMS_ID").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionUSRMS_ID", response.getJSONObject("data").getString("USRMS_ID"));
                                }
                                if (!response.getJSONObject("data").getString("USRMS_NAME").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionUSRMS_NAME", response.getJSONObject("data").getString("USRMS_NAME"));
                                }
                                if (!response.getJSONObject("data").getString("HR_CODE").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionHR_CODE", response.getJSONObject("data").getString("HR_CODE"));
                                }
                                if (!response.getJSONObject("data").getString("USERID_CODE").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionUSERID_CODE", response.getJSONObject("data").getString("USERID_CODE"));
                                }
                                if (!response.getJSONObject("data").getString("LOGIN_ID").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionLOGIN_ID", response.getJSONObject("data").getString("LOGIN_ID"));
                                }
                                if (!response.getJSONObject("data").getString("U_LOGIN_PS").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionU_LOGIN_PS", response.getJSONObject("data").getString("U_LOGIN_PS"));
                                }
                                if (!response.getJSONObject("data").getString("REG_CODE").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionREG_CODE", response.getJSONObject("data").getString("REG_CODE"));
                                }
                                if (!response.getJSONObject("data").getString("REG_NAME").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionREG_NAME", response.getJSONObject("data").getString("REG_NAME"));
                                }

                                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                System.out.print("Response Json :"+response.toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_error) + anError.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("Error: ", anError.toString());
                    }
                });
    }
    void LoginuserHRisAurh(String params) {
        AndroidNetworking.post(vApi.SERVER_API + "check_user_hris.php")
                .setOkHttpClient(WebServiceHelper.getSSLClient())
                .addBodyParameter("USRMS_ID", params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                if (!response.getJSONObject("data").getString("USRMS_ID").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionUSRMS_ID", response.getJSONObject("data").getString("USRMS_ID"));
                                }
                                if (!response.getJSONObject("data").getString("USRMS_NAME").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionUSRMS_NAME", response.getJSONObject("data").getString("USRMS_NAME"));
                                }
                                if (!response.getJSONObject("data").getString("HR_CODE").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionHR_CODE", response.getJSONObject("data").getString("HR_CODE"));
                                }
                                if (!response.getJSONObject("data").getString("USERID_CODE").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionUSERID_CODE", response.getJSONObject("data").getString("USERID_CODE"));
                                }
                                if (!response.getJSONObject("data").getString("LOGIN_ID").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionLOGIN_ID", response.getJSONObject("data").getString("LOGIN_ID"));
                                }
                                if (!response.getJSONObject("data").getString("U_LOGIN_PS").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionU_LOGIN_PS", response.getJSONObject("data").getString("U_LOGIN_PS"));
                                }
                                if (!response.getJSONObject("data").getString("REG_CODE").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionREG_CODE", response.getJSONObject("data").getString("REG_CODE"));
                                }
                                if (!response.getJSONObject("data").getString("REG_NAME").equals("null")) {
                                    utils.setDataSession(MainActivity.this, "sessionREG_NAME", response.getJSONObject("data").getString("REG_NAME"));
                                }

                                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                System.out.print("Response Json :"+response.toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.msg_error) + anError.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("Error: ", anError.toString());
                    }
                });
    }

    private class LoginHRIS extends AsyncTask<Void, Void, String>{
        ProgressDialog dialog = utils.showDialogBar(MainActivity.this,"Loading");
        String response = "";
        String status = "";
        String user_id = viewHRISAccount.getText().toString();
        String pasword =  viewHRISPassword.getText().toString();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                RequestBody body = new FormBody.Builder()
                        .add("uLogin",user_id)
                        .add("uPaswd",pasword)
                        .build();
                response = WebServiceHelper.doPOST(getResources().getString(R.string.login_hris),body,"");
                status = "OK";
            } catch (Exception e) {
                e.printStackTrace();
                status = "NOK";
            }
            return status;
        }

        @Override
        protected void onPostExecute(String status) {
            super.onPostExecute(status);
            if (status.equals("OK")){
                dialog.dismiss();
                String res = response.toString();
                XmlToJson xmlToJson = new XmlToJson.Builder(res).build();
                System.out.println(xmlToJson);
                LoginuserHRisAurh(user_id);

            }else if (status.equals("NOK")){
                dialog.dismiss();
//                startActivity(getIntent());
            }
        }
    }
    void inits(){
        final ProgressDialog dialog = utils.showDialogBar(MainActivity.this,"Loading");
        dialog.show();
        AndroidNetworking.post(getResources().getString(R.string.login_hris))
                .setOkHttpClient(SSLVSoket.getUnsafeOkHttpClient().build())
                .addBodyParameter("uLogin",viewHRISAccount.getText().toString())
                .addBodyParameter("uPaswd",viewHRISPassword.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        LoginuserHRisAurh(viewHRISAccount.getText().toString());
                        Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("Eroor : ",anError.getMessage());
                        dialog.dismiss();
                    }
                });
    }
}
