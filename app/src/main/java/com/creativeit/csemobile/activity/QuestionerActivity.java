package com.creativeit.csemobile.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.adapter.QstLvl2Adapter;
import com.creativeit.csemobile.gps_location.GPSTracking;
import com.creativeit.csemobile.helpers.crud_modules.Questioner;
import com.creativeit.csemobile.helpers.ssl.SSLVSoket;
import com.creativeit.csemobile.helpers.utils;
import com.creativeit.csemobile.model.ModelQstAll;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

public class QuestionerActivity extends AppCompatActivity {
    @BindView(R.id.viewRegQst)TextView viewRegQst;
    @BindView(R.id.viewQstLvl1)TextView viewQstLvl1;

    @BindView(R.id.viewNote_1)TextView viewNote_1;
    @BindView(R.id.viewNote_2)TextView viewNote_2;
    @BindView(R.id.viewFriendliness)TextView viewFriendliness;
    @BindView(R.id.viewGrade_A)TextView viewGrade_A;
    @BindView(R.id.viewGrade_B)TextView viewGrade_B;
    @BindView(R.id.viewGrade_C)TextView viewGrade_C;
    @BindView(R.id.viewHeadPenampilan)TextView viewHeadPenampilan;
    @BindView(R.id.viewValPenampilan)TextView viewValPenampilan;
    @BindView(R.id.viewHeadKeramahan)TextView viewHeadKeramahan;
    @BindView(R.id.viewValKeramahan)TextView viewValKeramahan;
    @BindView(R.id.viewValKeramahan2)TextView viewValKeramahan2;
    @BindView(R.id.viewHeadSuggestive_Selling)TextView viewHeadSuggestive_Selling;
    @BindView(R.id.viewValSuggestive_Selling)TextView viewValSuggestive_Selling;
    @BindView(R.id.viewHeadKecepatan_transakasi)TextView viewHeadKecepatan_transakasi;
    @BindView(R.id.viewValKecepatan_transakasi)TextView viewValKecepatan_transakasi;
    @BindView(R.id.viewHeadKejujuran_transakasi)TextView viewHeadKejujuran_transakasi;
    @BindView(R.id.viewValKejujuran_transakasi)TextView viewValKejujuran_transakasi;
    @BindView(R.id.viewValKejujuran_transakasi2)TextView viewValKejujuran_transakasi2;
    @BindView(R.id.viewTidiness)TextView viewTidiness;
    @BindView(R.id.viewNote_Tidiness_1)TextView viewNote_Tidiness_1;
    @BindView(R.id.viewNote_Tidiness_2)TextView viewNote_Tidiness_2;
    @BindView(R.id.viewGrade_Tidiness_A)TextView viewGrade_Tidiness_A;
    @BindView(R.id.viewGrade_Tidiness_B)TextView viewGrade_Tidiness_B;
    @BindView(R.id.viewGrade_Tidiness_C)TextView viewGrade_Tidiness_C;
    @BindView(R.id.viewHeadArea_Luar_Toko)TextView viewHeadArea_Luar_Toko;
    @BindView(R.id.viewValArea_Luar_Toko_QS06020101)TextView viewValArea_Luar_Toko_QS06020101;
    @BindView(R.id.viewValArea_Luar_Toko_QS06020102)TextView viewValArea_Luar_Toko_QS06020102;
    @BindView(R.id.viewHeadArea_Dalam_Toko)TextView viewHeadArea_Dalam_Toko;
    @BindView(R.id.viewValArea_Dalam_Toko_QS06020201)TextView viewValArea_Dalam_Toko_QS06020201;
    @BindView(R.id.viewValArea_Dalam_Toko_QS06020202)TextView viewValArea_Dalam_Toko_QS06020202;
    @BindView(R.id.viewValArea_Dalam_Toko_QS06020203)TextView viewValArea_Dalam_Toko_QS06020203;
    @BindView(R.id.viewValArea_Dalam_Toko_QS06020204)TextView viewValArea_Dalam_Toko_QS06020204;
    @BindView(R.id.viewHeadDisplay)TextView viewHeadDisplay;
    @BindView(R.id.viewValDisplay_QS06020301)TextView viewValDisplay_QS06020301;
    @BindView(R.id.viewValDisplay_QS06020302)TextView viewValDisplay_QS06020302;
    @BindView(R.id.viewValDisplay_QS06020303)TextView viewValDisplay_QS06020303;
    @BindView(R.id.viewValDisplay_QS06020304)TextView viewValDisplay_QS06020304;
    @BindView(R.id.viewHeadPromosi)TextView viewHeadPromosi;
    @BindView(R.id.viewValPromosi_QS06020401)TextView viewValPromosi_QS06020401;
    @BindView(R.id.viewValPromosi_QS06020402)TextView viewValPromosi_QS06020402;
    @BindView(R.id.viewHeadKebersihan_dan_higienitas)TextView viewHeadKebersihan_dan_higienitas;
    @BindView(R.id.viewValKebersihan_dan_higienitas_QS06020501)TextView viewValKebersihan_dan_higienitas_QS06020501;
    @BindView(R.id.viewValKebersihan_dan_higienitas_QS06020502)TextView viewValKebersihan_dan_higienitas_QS06020502;
    @BindView(R.id.viewHeadEquipment_Toko)TextView viewHeadEquipment_Toko;
    @BindView(R.id.viewValEquipment_Toko_QS06020601)TextView viewValEquipment_Toko_QS06020601;
    @BindView(R.id.viewHeadToilet_dan_Gudang)TextView viewHeadToilet_dan_Gudang;
    @BindView(R.id.viewValToilet_dan_Gudang_QS06020701)TextView viewValToilet_dan_Gudang_QS06020701;
    @BindView(R.id.viewFreshness)TextView viewFreshness;
    @BindView(R.id.viewNote_Freshness_1)TextView viewNote_Freshness_1;
    @BindView(R.id.viewNote_Freshness_2)TextView viewNote_Freshness_2;
    @BindView(R.id.viewGrade_Freshness_A)TextView viewGrade_Freshness_A;
    @BindView(R.id.viewGrade_Freshness_B)TextView viewGrade_Freshness_B;
    @BindView(R.id.viewGrade_Freshness_C)TextView viewGrade_Freshness_C;
    @BindView(R.id.viewHeadKualitas_Produk)TextView viewHeadKualitas_Produk;
    @BindView(R.id.viewValKualitas_Produk_QS06030101)TextView viewValKualitas_Produk_QS06030101;
    @BindView(R.id.viewValKualitas_Produk_QS06030102)TextView viewValKualitas_Produk_QS06030102;
    @BindView(R.id.viewValKualitas_Produk_QS06030103)TextView viewValKualitas_Produk_QS06030103;
    @BindView(R.id.viewValKualitas_Produk_QS06030104)TextView viewValKualitas_Produk_QS06030104;
    @BindView(R.id.viewRadioPenampilan)RadioGroup viewRadioPenampilan;
    @BindView(R.id.viewVRadioKeramahan_1)RadioGroup viewVRadioKeramahan_1;
    @BindView(R.id.viewVRadioKeramahan_2)RadioGroup viewVRadioKeramahan_2;
    @BindView(R.id.viewRadioSuggestive_Selling)RadioGroup viewRadioSuggestive_Selling;
    @BindView(R.id.viewRadioKecepatan_transakasi)RadioGroup viewRadioKecepatan_transakasi;
    @BindView(R.id.viewComment_lvl1)EditText viewComment_lvl1;
    @BindView(R.id.viewComment_lvl2)EditText viewComment_lvl2;
    @BindView(R.id.viewComment_lvl3)EditText viewComment_lvl3;
    BottomNavigationView btm_nav;
    Handler handler;
    String datemou = "";
    private JSONArray arr;
    private JSONObject obj;
    private List<ModelQstAll> list;
    private QstLvl2Adapter adapter;
    private String session = "session_Lvl2_";
    private String sessionLvl31 = "session_Lvl31_";
    private String sessionLvl32 = "sessionLvl32_";
    private String sessionLvl33 = "sessionLvl33_";

    GPSTracking gps;
    double latitude;
    double longitude;
    String CONV_LOC = "";
    String RadioPenampilan_String = "";
    String RadioKeramahan_String_I = "";
    String RadioKeramahan_String_II = "";
    String RadioSuggestive_Selling_String = "";
    String RadioKecepatan_transakasi_String = "";
    String RadioKejujuran_transakasi_String_I = "";
    String RadioKejujuran_transakasi_String_II = "";
    String RadioArea_Luar_Toko_QS06020101_String = "";
    String RadioArea_Luar_Toko_QS06020102_String = "";
    String RadioArea_Dalam_Toko_QS06020201_String = "";
    String RadioArea_Dalam_Toko_QS06020202_String = "";
    String RadioArea_Dalam_Toko_QS06020203_String = "";
    String RadioArea_Dalam_Toko_QS06020204_String = "";
    String RadioDisplay_QS06020301_String = "";
    String RadioDisplay_QS06020302_String = "";
    String RadioDisplay_QS06020303_String = "";
    String RadioDisplay_QS06020304_String = "";
    String RadioPromosi_QS06020401_String = "";
    String RadioPromosi_QS06020402_String = "";
    String RadioKebersihan_dan_higienitas_QS06020501_String = "";
    String RadioKebersihan_dan_higienitas_QS06020502_String = "";
    String RadioEquipment_Toko_QS0602060_String = "";
    String RadioToilet_dan_Gudang_QS06020701_String = "";
    String RadioKualitas_Produk_QS06030101_String = "";
    String RadioKualitas_Produk_QS06030102_String = "";
    String RadioKualitas_Produk_QS060301031_String = "";
    String RadioKualitas_Produk_QS06030104_String = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questioner);
        ButterKnife.bind(this);
        init();
        getGPS();
        addGpsLocation();
    }
    @OnCheckedChanged({R.id.viewRadioPenampilan_Yes,R.id.viewRadioPenampilan_No})
    public void viewRadioPenampilan(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioPenampilan_Yes:
                    RadioPenampilan_String = utils.getDataSession(QuestionerActivity.this,"session_Lvl31_QST_POINT_LVL_4_3");
                    Toast.makeText(getApplicationContext(),"Yes : "+RadioPenampilan_String,Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioPenampilan_No:
                    RadioPenampilan_String = "0";
                    Toast.makeText(getApplicationContext(),"No : "+RadioPenampilan_String,Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewVRadioKeramahan_Yes_1,R.id.viewVRadioKeramahan_No_1})
    public void viewVRadioKeramahan_1(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewVRadioKeramahan_Yes_1:
                    RadioKeramahan_String_I = utils.getDataSession(QuestionerActivity.this,"session_Lvl31_QST_POINT_LVL_4_6");
                    Toast.makeText(getApplicationContext(),"Yes : "+RadioKeramahan_String_I,Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewVRadioKeramahan_No_1:
                    RadioKeramahan_String_I = "0";
                    Toast.makeText(getApplicationContext(),"No : ",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewVRadioKeramahan_Yes_2,R.id.viewVRadioKeramahan_No_2})
    public void viewVRadioKeramahan_2(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewVRadioKeramahan_Yes_2:
                    RadioKeramahan_String_II = utils.getDataSession(QuestionerActivity.this,"session_Lvl31_QST_POINT_LVL_4_5");
                    Toast.makeText(getApplicationContext(),"Yes : "+RadioKeramahan_String_II,Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewVRadioKeramahan_No_2:
                    RadioKeramahan_String_II = "0";
                    Toast.makeText(getApplicationContext(),"No : "+RadioKeramahan_String_II,Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
    @OnCheckedChanged({R.id.viewRadioSuggestive_Selling_Yes,R.id.viewRadioSuggestive_Selling_No})
    public void viewRadioSuggestive_Selling(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioSuggestive_Selling_Yes:
                    RadioSuggestive_Selling_String = utils.getDataSession(QuestionerActivity.this,"ession_Lvl31_QST_POINT_LVL_4_8");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioSuggestive_Selling_No:
                    RadioSuggestive_Selling_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKecepatan_transakasi_Yes,R.id.viewRadioKecepatan_transakasi_No})
    public void viewRadioKecepatan_transakasi(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKecepatan_transakasi_Yes:
                    RadioKecepatan_transakasi_String = utils.getDataSession(QuestionerActivity.this,"session_Lvl31_QST_POINT_LVL_4_3");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKecepatan_transakasi_No:
                    RadioKecepatan_transakasi_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKejujuran_transakasi_Yes_1,R.id.viewRadioKejujuran_transakasi_No_1})
    public void viewRadioKejujuran_transakasi_1(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKejujuran_transakasi_Yes_1:
                    RadioKejujuran_transakasi_String_I = utils.getDataSession(QuestionerActivity.this,"session_Lvl31_QST_POINT_LVL_4_4");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKejujuran_transakasi_No_1:
                    RadioKejujuran_transakasi_String_I = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKejujuran_transakasi_Yes_2,R.id.viewRadioKejujuran_transakasi_No_2})
    public void viewRadioKejujuran_transakasi_2(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKejujuran_transakasi_Yes_2:
                    RadioKejujuran_transakasi_String_II = utils.getDataSession(QuestionerActivity.this,"session_Lvl31_QST_POINT_LVL_4_6");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKejujuran_transakasi_No_2:
                    RadioKejujuran_transakasi_String_II = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioArea_Luar_Toko_QS06020101_Yes_1,R.id.viewRadioArea_Luar_Toko_QS06020101_No_1})
    public void viewRadioArea_Luar_Toko_QS06020101_1(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioArea_Luar_Toko_QS06020101_Yes_1:
                    RadioArea_Luar_Toko_QS06020101_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_3");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioArea_Luar_Toko_QS06020101_No_1:
                    RadioArea_Luar_Toko_QS06020101_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }


    @OnCheckedChanged({R.id.viewRadioArea_Luar_Toko_QS06020102_Yes,R.id.viewRadioArea_Luar_Toko_QS06020102_No})
    public void viewRadioArea_Luar_Toko_QS06020102(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioArea_Luar_Toko_QS06020102_Yes:
                    RadioArea_Luar_Toko_QS06020102_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_2");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioArea_Luar_Toko_QS06020102_No:
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }


    @OnCheckedChanged({R.id.viewRadioArea_Dalam_Toko_QS06020201_yes,R.id.viewRadioArea_Dalam_Toko_QS06020201_No})
    public void viewRadioArea_Dalam_Toko_QS06020201(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioArea_Dalam_Toko_QS06020201_yes:
                    RadioArea_Dalam_Toko_QS06020201_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_2");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioArea_Dalam_Toko_QS06020201_No:
                    RadioArea_Dalam_Toko_QS06020201_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioArea_Dalam_Toko_QS06020202_Yes,R.id.viewRadioArea_Dalam_Toko_QS06020202_No})
    public void viewRadioArea_Dalam_Toko_QS06020202(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioArea_Dalam_Toko_QS06020202_Yes:
                    RadioArea_Dalam_Toko_QS06020202_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_3");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioArea_Dalam_Toko_QS06020202_No:
                    RadioArea_Dalam_Toko_QS06020202_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioArea_Dalam_Toko_QS06020203_Yes,R.id.viewRadioArea_Dalam_Toko_QS06020203_No})
    public void viewRadioArea_Dalam_Toko_QS06020203(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioArea_Dalam_Toko_QS06020203_Yes:
                    RadioArea_Dalam_Toko_QS06020203_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_4");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioArea_Dalam_Toko_QS06020203_No:
                    RadioArea_Dalam_Toko_QS06020203_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioArea_Dalam_Toko_QS06020204_Yes,R.id.viewRadioArea_Dalam_Toko_QS06020204_No})
    public void viewRadioArea_Dalam_Toko_QS06020204(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioArea_Dalam_Toko_QS06020204_Yes:
                    RadioArea_Dalam_Toko_QS06020204_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_6");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioArea_Dalam_Toko_QS06020204_No:
                    RadioArea_Dalam_Toko_QS06020204_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioDisplay_QS06020301_Yes,R.id.viewRadioDisplay_QS06020301_No})
    public void viewRadioDisplay_QS06020301(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioDisplay_QS06020301_Yes:
                    RadioDisplay_QS06020301_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_4");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioDisplay_QS06020301_No:
                    RadioDisplay_QS06020301_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioDisplay_QS06020302_Yes,R.id.viewRadioDisplay_QS06020302_No})
    public void viewRadioDisplay_QS06020302(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioDisplay_QS06020302_Yes:
                    RadioDisplay_QS06020302_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_7");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioDisplay_QS06020302_No:
                    RadioDisplay_QS06020302_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioDisplay_QS06020303_Yes,R.id.viewRadioDisplay_QS06020303_No})
    public void viewRadioDisplay_QS06020303(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioDisplay_QS06020303_Yes:
                    RadioDisplay_QS06020303_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_2");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioDisplay_QS06020303_No:
                    RadioDisplay_QS06020303_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioDisplay_QS06020304_Yes,R.id.viewRadioDisplay_QS06020304_No})
    public void viewRadioDisplay_QS06020304(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioDisplay_QS06020304_Yes:
                    RadioDisplay_QS06020304_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_2");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioDisplay_QS06020304_No:
                    RadioDisplay_QS06020304_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioPromosi_QS06020401_Yes,R.id.viewRadioPromosi_QS06020401_No})
    public void viewRadioPromosi_QS06020401(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioPromosi_QS06020401_Yes:
                    RadioPromosi_QS06020401_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_3");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioPromosi_QS06020401_No:
                    RadioPromosi_QS06020401_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioPromosi_QS06020402_Yes,R.id.viewRadioPromosi_QS06020402_No})
    public void viewRadioPromosi_QS06020402(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioPromosi_QS06020402_Yes:
                    RadioPromosi_QS06020402_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_2");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioPromosi_QS06020402_No:
                    RadioPromosi_QS06020402_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKebersihan_dan_higienitas_QS06020501_Yes,R.id.viewRadioKebersihan_dan_higienitas_QS06020501_No})
    public void viewRadioKebersihan_dan_higienitas_QS06020501(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKebersihan_dan_higienitas_QS06020501_Yes:
                    RadioKebersihan_dan_higienitas_QS06020501_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_6");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKebersihan_dan_higienitas_QS06020501_No:
                    RadioKebersihan_dan_higienitas_QS06020501_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKebersihan_dan_higienitas_QS06020502_Yes,R.id.viewRadioKebersihan_dan_higienitas_QS06020502_No})
    public void viewRadioKebersihan_dan_higienitas_QS06020502(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKebersihan_dan_higienitas_QS06020502_Yes:
                    RadioKebersihan_dan_higienitas_QS06020502_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_2");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKebersihan_dan_higienitas_QS06020502_No:
                    RadioKebersihan_dan_higienitas_QS06020502_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }


    @OnCheckedChanged({R.id.viewRadioEquipment_Toko_QS0602060_Yes,R.id.viewRadioEquipment_Toko_QS0602060_No})
    public void viewRadioEquipment_Toko_QS0602060(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioEquipment_Toko_QS0602060_Yes:
                    RadioEquipment_Toko_QS0602060_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_6");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioEquipment_Toko_QS0602060_No:
                    RadioEquipment_Toko_QS0602060_String = "0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioToilet_dan_Gudang_QS06020701_Yes,R.id.viewRadioToilet_dan_Gudang_QS06020701_No})
    public void viewRadioToilet_dan_Gudang_QS06020701(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioToilet_dan_Gudang_QS06020701_Yes:
                    RadioToilet_dan_Gudang_QS06020701_String = utils.getDataSession(QuestionerActivity.this,"sessionLvl32_QST_POINT_LVL_4_3");
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioToilet_dan_Gudang_QS06020701_No:
                    RadioToilet_dan_Gudang_QS06020701_String ="0";
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }


    @OnCheckedChanged({R.id.viewRadioKualitas_Produk_QS06030101_Yes,R.id.viewRadioKualitas_Produk_QS06030101_No})
    public void viewRadioKualitas_Produk_QS06030101(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKualitas_Produk_QS06030101_Yes:
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKualitas_Produk_QS06030101_No:
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKualitas_Produk_QS06030102_Yes,R.id.viewRadioKualitas_Produk_QS06030102_No})
    public void viewRadioKualitas_Produk_QS06030102(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKualitas_Produk_QS06030102_Yes:
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKualitas_Produk_QS06030102_No:
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKualitas_Produk_QS06030103_Yes,R.id.viewRadioKualitas_Produk_QS06030103_No})
    public void viewRadioKualitas_Produk_QS06030103(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKualitas_Produk_QS06030103_Yes:
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKualitas_Produk_QS06030103_No:
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.viewRadioKualitas_Produk_QS06030104_Yes,R.id.viewRadioKualitas_Produk_QS06030104_No})
    public void viewRadioKualitas_Produk_QS06030104(CompoundButton button, boolean checkedId){
        if (checkedId){
            switch (button.getId()){
                case R.id.viewRadioKualitas_Produk_QS06030104_Yes:
                    Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                    break;
                case R.id.viewRadioKualitas_Produk_QS06030104_No:
                    Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @SuppressLint("SetTextI18n")
    void init() {

        Runnable runnable = new Runnable() {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void run() {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
                datemou = format.format(c.getTime());
                utils.setDataSession(QuestionerActivity.this, "sessionDateYear", datemou);
            }
        };
        handler = new Handler();
        handler.postDelayed(runnable, 1000);
        viewRegQst.setText(utils.getDataSession(this, "sessionvRegion") + " - " + utils.getDataSession(this, "sessionStore"));

        showHeadirList();
        loadLvl1(utils.getDataSession(this, "sessionQstData"));
        QstLevel31(utils.getDataSession(this, "sessionQstData"));
        QstLevel32(utils.getDataSession(this, "sessionQstData"));
        QstLevel33(utils.getDataSession(this, "sessionQstData"));
        QstLvl2Session(utils.getDataSession(this, "sessionQstData"));

        viewFriendliness.setText(utils.getDataSession(this, "session_Lvl2_QST_CAPTION_Friendliness"));
        viewNote_1.setText("  " + utils.getDataSession(this, "session_Lvl2_QST_NOTE_1_QS0601"));
        viewNote_2.setText("  " + utils.getDataSession(this, "session_Lvl2_QST_NOTE_2_QS0601"));

        viewGrade_A.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_A_A") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_A_30") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_A_35"));
        viewGrade_B.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_B_B") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_B_24") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_B_29"));
        viewGrade_C.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_C_C") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_C_0") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_C_23"));

        viewHeadPenampilan.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_3_Penampilan"));
        viewValPenampilan.setText(" " + utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010101"));

        viewValPenampilan.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010101"));

        viewHeadKeramahan.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_3_Keramahan"));
        viewValKeramahan.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010201"));
        viewValKeramahan2.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010202"));

        viewHeadSuggestive_Selling.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_3_Suggestive Selling"));
        viewValSuggestive_Selling.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010301"));

        viewHeadKecepatan_transakasi.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_3_Kecepatan Transaksi"));
        viewValKecepatan_transakasi.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010401"));

        viewHeadKejujuran_transakasi.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_3_Kejujuran Transaksi"));
        viewValKejujuran_transakasi.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010501"));
        viewValKejujuran_transakasi2.setText(utils.getDataSession(this,"session_Lvl31_QST_CAPTION_LVL_4_QS06010502"));

        viewTidiness.setText(utils.getDataSession(this,"session_Lvl2_QST_CAPTION_Tidiness"));
        viewNote_Tidiness_1.setText(utils.getDataSession(this,"session_Lvl2_QST_NOTE_1_QS0602"));
        viewNote_Tidiness_2.setText(utils.getDataSession(this,"session_Lvl2_QST_NOTE_2_QS0602"));

        viewGrade_Tidiness_A.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_A_A") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_A_52") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_A_57"));
        viewGrade_Tidiness_B.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_B_B") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_B_46") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_B_51"));
        viewGrade_Tidiness_C.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_C_C") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_C_0") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_C_45"));
        viewHeadArea_Luar_Toko.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_3_Area Luar Toko"));
        viewValArea_Luar_Toko_QS06020101.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020101"));
        viewValArea_Luar_Toko_QS06020102.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020102"));

        viewHeadArea_Dalam_Toko.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_3_Area Dalam Toko"));
        viewValArea_Dalam_Toko_QS06020201.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020201"));
        viewValArea_Dalam_Toko_QS06020202.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020202"));
        viewValArea_Dalam_Toko_QS06020203.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020203"));
        viewValArea_Dalam_Toko_QS06020204.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020204"));

        viewHeadDisplay.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_3_Display"));
        viewValDisplay_QS06020301.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020301"));
        viewValDisplay_QS06020302.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020302"));
        viewValDisplay_QS06020303.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020303"));
        viewValDisplay_QS06020304.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020304"));

        viewHeadPromosi.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_3_Promosi"));
        viewValPromosi_QS06020401.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020401"));
        viewValPromosi_QS06020402.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020402"));

        viewHeadKebersihan_dan_higienitas.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_3_Kebersihan dan higienitas"));
        viewValKebersihan_dan_higienitas_QS06020501.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020501"));
        viewValKebersihan_dan_higienitas_QS06020502.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020502"));

        viewHeadEquipment_Toko.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_3_Equipment Toko"));
        viewValEquipment_Toko_QS06020601.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020601"));

        viewHeadToilet_dan_Gudang.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_3_Toilet & Gudang"));
        viewValToilet_dan_Gudang_QS06020701.setText(utils.getDataSession(this,"sessionLvl32_QST_CAPTION_LVL_4_QS06020701"));

        viewFreshness.setText(utils.getDataSession(this,"session_Lvl2_QST_CAPTION_Freshness"));
        viewNote_Freshness_1.setText(utils.getDataSession(this,"session_Lvl2_QST_NOTE_1_QS0603"));
        viewNote_Freshness_2.setText(utils.getDataSession(this,"session_Lvl2_QST_NOTE_2_QS0603"));

        viewGrade_Freshness_A.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_A_A") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_A_7") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_A_8"));
        viewGrade_Freshness_B.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_B_B") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_B_4") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_B_6"));
        viewGrade_Freshness_C.setText(utils.getDataSession(this, "session_Lvl2_QST_GRADE_C_C") + " : " + utils.getDataSession(this, "session_Lvl2_MIN_GRADE_C_0") + " - " + utils.getDataSession(this, "session_Lvl2_MAX_GRADE_C_3"));

        viewHeadKualitas_Produk.setText(utils.getDataSession(this, "sessionLvl33_QST_CAPTION_LVL_3_Kualitas Produk"));
        viewValKualitas_Produk_QS06030101.setText(utils.getDataSession(this, "sessionLvl33_QST_CAPTION_LVL_4_QS06030101"));
        viewValKualitas_Produk_QS06030102.setText(utils.getDataSession(this, "sessionLvl33_QST_CAPTION_LVL_4_QS06030102"));
        viewValKualitas_Produk_QS06030103.setText(utils.getDataSession(this, "sessionLvl33_QST_CAPTION_LVL_4_QS06030103"));
        viewValKualitas_Produk_QS06030104.setText(utils.getDataSession(this, "sessionLvl33_QST_CAPTION_LVL_4_QS06030104"));

        btm_nav = (BottomNavigationView) findViewById(R.id.btm_nav);
        btm_nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.view_preview:
                        Toast.makeText(getApplicationContext(), utils.getDataSession(QuestionerActivity.this, "session_Lvl2_QST_CAPTION_Tidiness"), Toast.LENGTH_LONG).show();
                        break;
                    case R.id.viewReset:
                        break;
                    case R.id.viewMenu:
                        break;
                    case R.id.viewLogout:
                        break;
                }
                return true;
            }
        });
    }

    void getGPS(){
        gps = new GPSTracking(this);
        if (gps.canGetLocation()){
            latitude = gps.getLatitude();
            longitude= gps.getLongitude();
            Log.d("Location : ", String.valueOf(latitude));
            Log.d("Location : ", String.valueOf(longitude));
            Log.d("Location : ",gps.formatLatLongGPSTRACKING(latitude,longitude));
            CONV_LOC = gps.formatLatLongGPSTRACKING(latitude,longitude);
        }else{
            gps.showSettingAlerts();
        }
    }

    void addGpsLocation(){
        AndroidNetworking.post(getResources().getString(R.string.take_loaction))
                .setOkHttpClient(SSLVSoket.getUnsafeOkHttpClient().build())
                .addBodyParameter("USRMS_ID",utils.getDataSession(this,"sessionUSRMS_ID"))
                .addBodyParameter("LATITUDE", String.valueOf(latitude))
                .addBodyParameter("LONGITUDE", String.valueOf(longitude))
                .addBodyParameter("LOCATION_CONVT","")
                .addBodyParameter("USRMS_NAME",utils.getDataSession(this,"sessionUSRMS_NAME"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                Toast.makeText(getApplicationContext(),"Take Location Successfully", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(),"Take Location An Error "+ anError.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("an Error : ",anError.getMessage());
                    }
                });
    }

    void showHeadirList() {
        AndroidNetworking.get(getResources().getString(R.string.head_list)).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                arr = response.getJSONArray("data");
                                for (int i = 0; i < arr.length(); i++) {
                                    obj = arr.getJSONObject(i);
                                    utils.setDataSession(QuestionerActivity.this, "sessionQstData", obj.getString("QST_ID"));
                                    Log.e("In Head :", obj.getString("QST_ID"));
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Response False", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.fillInStackTrace();
                            Log.e("Opps Error : ", e.toString());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), "Opps Error", Toast.LENGTH_LONG).show();
                    }
                });
    }

    void loadLvl1(String params) {
        AndroidNetworking.post(getResources().getString(R.string.qst_level_1))
                .addBodyParameter("P_QST_ID", params).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                JSONArray varr = response.getJSONArray("data");
                                for (int i = 0; i < varr.length(); i++) {
                                    obj = varr.getJSONObject(i);
                                    viewQstLvl1.setText(utils.getDataSession(QuestionerActivity.this, "sessionDateYear") + "-" + obj.getString("QST_ID") + "-" + obj.getString("QST_CAPTION"));
                                    Log.e("In Level 1 :", obj.getString("QST_ID"));
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Response False", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.fillInStackTrace();
                            Log.e("Opps Error :", e.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), "Opps Error", Toast.LENGTH_LONG).show();
                    }
                });
    }

    void loadLvl2(String params) {
        AndroidNetworking.post(getResources().getString(R.string.qst_level_2))
                .addBodyParameter("P_QST_ID", params).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                Log.e("Response", response.toString());

                                list = new ArrayList<ModelQstAll>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    ModelQstAll md = new ModelQstAll();
                                    obj = array.getJSONObject(i);
                                    md.setP_QST_ID(obj.getString("P_QST_ID"));
                                    md.setQST_ID(obj.getString("QST_ID"));
                                    md.setQST_LVL(obj.getString("QST_LVL"));
                                    md.setQST_CAPTION(obj.getString("QST_CAPTION"));
                                    md.setQST_NOTE_1(obj.getString("QST_NOTE_1"));
                                    md.setQST_NOTE_2(obj.getString("QST_NOTE_2"));
                                    md.setQST_NOTE_3(obj.getString("QST_NOTE_3"));
                                    md.setQST_NOTE_4(obj.getString("QST_NOTE_4"));
                                    md.setQST_LIST_TYPE(obj.getString("QST_LIST_TYPE"));
                                    md.setQST_GRADE_A(obj.getString("QST_GRADE_A"));
                                    md.setMIN_GRADE_A(obj.getString("MIN_GRADE_A"));
                                    md.setMAX_GRADE_A(obj.getString("MAX_GRADE_A"));

                                    md.setQST_GRADE_B(obj.getString("QST_GRADE_B"));
                                    md.setMIN_GRADE_B(obj.getString("MIN_GRADE_B"));
                                    md.setMAX_GRADE_B(obj.getString("MAX_GRADE_B"));

                                    md.setQST_GRADE_C(obj.getString("QST_GRADE_C"));
                                    md.setMIN_GRADE_C(obj.getString("MIN_GRADE_C"));
                                    md.setMAX_GRADE_C(obj.getString("MAX_GRADE_C"));

                                    md.setQST_GRADE_D(obj.getString("QST_GRADE_D"));
                                    md.setMIN_GRADE_D(obj.getString("MIN_GRADE_D"));
                                    md.setMAX_GRADE_D(obj.getString("MAX_GRADE_D"));

                                    JSONArray array2 = response.getJSONArray("res");
                                    for (int a = 0; a < array2.length(); a++) {

                                    }
                                    list.add(md);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    void QstLevel31(String params) {
        AndroidNetworking.post(getResources().getString(R.string.qst_level_3_1))
                .addBodyParameter("QST_ID", params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                JSONArray array = response.getJSONArray("data");
                                for (int i =0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    utils.setDataSession(QuestionerActivity.this, sessionLvl31 + "P_QST_LVL3_" + object.getString("P_QST_LVL3"), object.getString("P_QST_LVL3"));
                                    Log.d(" SESSION P_QST_LVL3 :", sessionLvl31 + "P_QST_LVL3_" + object.getString("P_QST_LVL3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl31 + "QST_CAPTION_LVL_3_" + object.getString("QST_CAPTION_LVL_3"), object.getString("QST_CAPTION_LVL_3"));
                                    Log.d(" S QST_CAPTION_LVL_3 :", sessionLvl31 + "QST_CAPTION_LVL_3_" + object.getString("QST_CAPTION_LVL_3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl31 + "QST_TYPE_LVL_3_" + object.getString("QST_TYPE_LVL_3"), object.getString("QST_TYPE_LVL_3"));
                                    Log.d(" S QST_TYPE_LVL_3 :", sessionLvl31 + "QST_TYPE_LVL_3_" + object.getString("QST_TYPE_LVL_3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl31 + "QST_ID_LVL_4_" + object.getString("QST_CAPTION_LVL_3"), object.getString("QST_ID_LVL_4"));
                                    Log.d(" SESSION QST_ID_LVL_4 :", sessionLvl31 + "QST_ID_LVL_4_" + object.getString("QST_ID_LVL_4"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl31 + "QST_CAPTION_LVL_4_" + object.getString("QST_ID_LVL_4"), object.getString("QST_CAPTION_LVL_4"));
                                    Log.d(" S QST_CAPTION_LVL_4 :", sessionLvl31 + "QST_CAPTION_LVL_4_" + object.getString("QST_ID_LVL_4"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl31 + "QST_TYPE_LVL_4_" + object.getString("QST_TYPE_LVL_4"), object.getString("QST_TYPE_LVL_4"));
                                    Log.d(" S QST_TYPE_LVL_4 : ", sessionLvl31 + "QST_TYPE_LVL_4_" + object.getString("QST_TYPE_LVL_4"));

                                    utils.setDataSession(QuestionerActivity.this, sessionLvl31 + "QST_POINT_LVL_4_" + object.getString("QST_POINT_LVL_4"), object.getString("QST_POINT_LVL_4"));
                                    Log.d(" S QST_POINT_LVL_4 : ", sessionLvl31 + "QST_POINT_LVL_4_" + object.getString("QST_POINT_LVL_4"));

                                    System.out.println("============================================31=================================================");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext()," 31 : "+anError.getMessage(),Toast.LENGTH_LONG).show();
                        System.out.println("Error 31: "+anError.getMessage());
                    }
                });
    }

    void QstLevel32(String params) {
        AndroidNetworking.post(getResources().getString(R.string.qst_level_3_2))
                .addBodyParameter("QST_ID", params).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    utils.setDataSession(QuestionerActivity.this, sessionLvl32 + "P_QST_LVL3_" + object.getString("P_QST_LVL3"), object.getString("P_QST_LVL3"));
                                    Log.d(" SESSION P_QST_LVL3 :", sessionLvl32 + "P_QST_LVL3_" + object.getString("P_QST_LVL3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl32 + "QST_CAPTION_LVL_3_" + object.getString("QST_CAPTION_LVL_3"), object.getString("QST_CAPTION_LVL_3"));
                                    Log.d(" S QST_CAPTION_LVL_3 :", sessionLvl32 + "QST_CAPTION_LVL_3_" + object.getString("QST_CAPTION_LVL_3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl32 + "QST_TYPE_LVL_3_" + object.getString("QST_TYPE_LVL_3"), object.getString("QST_TYPE_LVL_3"));
                                    Log.d(" S QST_TYPE_LVL_3 :", sessionLvl32 + "QST_TYPE_LVL_3_" + object.getString("QST_TYPE_LVL_3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl32 + "QST_ID_LVL_4_" + object.getString("QST_ID_LVL_4"), object.getString("QST_ID_LVL_4"));
                                    Log.d(" SESSION QST_ID_LVL_4 :", sessionLvl32 + "QST_ID_LVL_4_" + object.getString("QST_ID_LVL_4"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl32 + "QST_CAPTION_LVL_4_" + object.getString("QST_ID_LVL_4"), object.getString("QST_CAPTION_LVL_4"));
                                    Log.d(" S QST_CAPTION_LVL_4 :", sessionLvl32 + "QST_CAPTION_LVL_4_" + object.getString("QST_ID_LVL_4"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl32 + "QST_TYPE_LVL_4_" + object.getString("QST_TYPE_LVL_4"), object.getString("QST_TYPE_LVL_4"));
                                    Log.d(" S QST_TYPE_LVL_4 : ", sessionLvl32 + "QST_TYPE_LVL_4_" + object.getString("QST_TYPE_LVL_4"));

                                    utils.setDataSession(QuestionerActivity.this, sessionLvl32 + "QST_POINT_LVL_4_" + object.getString("QST_POINT_LVL_4"), object.getString("QST_POINT_LVL_4"));
                                    Log.d(" S QST_POINT_LVL_4 : ", sessionLvl32 + "QST_POINT_LVL_4_" + object.getString("QST_POINT_LVL_4"));

                                    System.out.println("=============================================================================================");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    void QstLevel33(String params) {
        AndroidNetworking.post(getResources().getString(R.string.qst_level_3_3))
                .addBodyParameter("QST_ID", params).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    utils.setDataSession(QuestionerActivity.this, sessionLvl33 + "P_QST_LVL3_" + object.getString("P_QST_LVL3"), object.getString("P_QST_LVL3"));
                                    Log.d(" SESSION P_QST_LVL3 :", sessionLvl33 + "P_QST_LVL3_" + object.getString("P_QST_LVL3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl33 + "QST_CAPTION_LVL_3_" + object.getString("QST_CAPTION_LVL_3"), object.getString("QST_CAPTION_LVL_3"));
                                    Log.d(" S QST_CAPTION_LVL_3 :", sessionLvl33 + "QST_CAPTION_LVL_3_" + object.getString("QST_CAPTION_LVL_3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl33 + "QST_TYPE_LVL_3_" + object.getString("QST_TYPE_LVL_3"), object.getString("QST_TYPE_LVL_3"));
                                    Log.d(" S QST_TYPE_LVL_3 :", sessionLvl33 + "QST_TYPE_LVL_3_" + object.getString("QST_TYPE_LVL_3"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl33 + "QST_ID_LVL_4_" + object.getString("QST_ID_LVL_4"), object.getString("QST_ID_LVL_4"));
                                    Log.d(" SESSION QST_ID_LVL_4 :", sessionLvl33 + "QST_ID_LVL_4_" + object.getString("QST_ID_LVL_4"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl33 + "QST_CAPTION_LVL_4_" + object.getString("QST_ID_LVL_4"), object.getString("QST_CAPTION_LVL_4"));
                                    Log.d(" S QST_CAPTION_LVL_4 :", sessionLvl33 + "QST_CAPTION_LVL_4_" + object.getString("QST_ID_LVL_4"));


                                    utils.setDataSession(QuestionerActivity.this, sessionLvl33 + "QST_TYPE_LVL_4_" + object.getString("QST_TYPE_LVL_4"), object.getString("QST_TYPE_LVL_4"));
                                    Log.d(" S QST_TYPE_LVL_4 : ", sessionLvl33 + "QST_TYPE_LVL_4_" + object.getString("QST_TYPE_LVL_4"));

                                    utils.setDataSession(QuestionerActivity.this, sessionLvl33 + "QST_POINT_LVL_4_" + object.getString("QST_POINT_LVL_4"), object.getString("QST_POINT_LVL_4"));
                                    Log.d(" S QST_POINT_LVL_4 : ", sessionLvl33 + "QST_POINT_LVL_4_" + object.getString("QST_POINT_LVL_4"));

                                    System.out.println("=============================================================================================");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    void QstLvl2Session(String params) {
        AndroidNetworking.post(getResources().getString(R.string.qst_level_2))
                .addBodyParameter("P_QST_ID", params).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                Log.e("Response", response.toString());
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    obj = array.getJSONObject(i);
                                    utils.setDataSession(QuestionerActivity.this, session + "P_QST_ID_" + obj.getString("P_QST_ID"), obj.getString("P_QST_ID"));
                                    Log.d(" SESSION P_QST_ID :", session + "P_QST_ID_" + obj.getString("P_QST_ID"));

                                    utils.setDataSession(QuestionerActivity.this, session + "QST_ID_" + obj.getString("QST_ID"), obj.getString("QST_ID"));
                                    Log.d(" SESSION QST_ID :", session + "QST_ID_" + obj.getString("QST_ID"));

                                    utils.setDataSession(QuestionerActivity.this, session + "QST_LVL_" + obj.getString("QST_LVL"), obj.getString("QST_LVL"));
                                    Log.d(" SESSION QST_LVL :", session + "QST_LVL_" + obj.getString("QST_LVL"));

                                    utils.setDataSession(QuestionerActivity.this, session + "QST_CAPTION_" + obj.getString("QST_CAPTION"), obj.getString("QST_CAPTION"));
                                    Log.d(" SESSION QST_CAPTION :", session + "QST_CAPTION_" + obj.getString("QST_CAPTION"));


                                    utils.setDataSession(QuestionerActivity.this, session + "QST_NOTE_1_" + obj.getString("QST_ID"), obj.getString("QST_NOTE_1"));
                                    Log.d(" SESSION QST_NOTE_1 :", session + "QST_NOTE_1_" + obj.getString("QST_ID"));

                                    utils.setDataSession(QuestionerActivity.this, session + "QST_NOTE_2_" + obj.getString("QST_ID"), obj.getString("QST_NOTE_2"));
                                    Log.d(" SESSION QST_NOTE_2 :", session + "QST_NOTE_2_" + obj.getString("QST_ID"));

                                    utils.setDataSession(QuestionerActivity.this, session + "QST_NOTE_3_" + obj.getString("QST_ID"), obj.getString("QST_NOTE_3"));
                                    Log.d(" SESSION QST_NOTE_3 :", session + "QST_NOTE_3_" + obj.getString("QST_ID"));

                                    utils.setDataSession(QuestionerActivity.this, session + "QST_NOTE_4_" + obj.getString("QST_ID"), obj.getString("QST_NOTE_4"));
                                    Log.d(" SESSION QST_NOTE_4 :", session + "QST_NOTE_4_" + obj.getString("QST_ID"));

                                    utils.setDataSession(QuestionerActivity.this, session + "QST_LIST_TYPE_" + obj.getString("QST_LIST_TYPE"), obj.getString("QST_LIST_TYPE"));
                                    Log.d("SESSION QST_LIST_TYPE :", session + "QST_LIST_TYPE_" + obj.getString("QST_LIST_TYPE"));


                                    utils.setDataSession(QuestionerActivity.this, session + "QST_GRADE_A_" + obj.getString("QST_GRADE_A"), obj.getString("QST_GRADE_A"));
                                    Log.d("SESSION QST_GRADE_A :", session + "QST_GRADE_A_" + obj.getString("QST_GRADE_A"));


                                    utils.setDataSession(QuestionerActivity.this, session + "MIN_GRADE_A_" + obj.getString("MIN_GRADE_A"), obj.getString("MIN_GRADE_A"));
                                    Log.d("SESSION MIN_GRADE_A :", session + "MIN_GRADE_A_" + obj.getString("MIN_GRADE_A"));


                                    utils.setDataSession(QuestionerActivity.this, session + "MAX_GRADE_A_" + obj.getString("MAX_GRADE_A"), obj.getString("MAX_GRADE_A"));
                                    Log.d("SESSION MAX_GRADE_A :", session + "MAX_GRADE_A_" + obj.getString("MAX_GRADE_A"));

                                    //B
                                    utils.setDataSession(QuestionerActivity.this, session + "QST_GRADE_B_" + obj.getString("QST_GRADE_B"), obj.getString("QST_GRADE_B"));
                                    Log.d("SESSION QST_GRADE_B :", session + "QST_GRADE_B_" + obj.getString("QST_GRADE_B"));


                                    utils.setDataSession(QuestionerActivity.this, session + "MIN_GRADE_B_" + obj.getString("MIN_GRADE_B"), obj.getString("MIN_GRADE_B"));
                                    Log.d("SESSION MIN_GRADE_B :", session + "MIN_GRADE_B_" + obj.getString("MIN_GRADE_B"));


                                    utils.setDataSession(QuestionerActivity.this, session + "MAX_GRADE_B_" + obj.getString("MAX_GRADE_B"), obj.getString("MAX_GRADE_B"));
                                    Log.d("SESSION MAX_GRADE_B :", session + "MAX_GRADE_B_" + obj.getString("MAX_GRADE_B"));

                                    // C
                                    utils.setDataSession(QuestionerActivity.this, session + "QST_GRADE_C_" + obj.getString("QST_GRADE_C"), obj.getString("QST_GRADE_C"));
                                    Log.d("SESSION QST_GRADE_C :", session + "QST_GRADE_C_" + obj.getString("QST_GRADE_C"));


                                    utils.setDataSession(QuestionerActivity.this, session + "MIN_GRADE_C_" + obj.getString("MIN_GRADE_C"), obj.getString("MIN_GRADE_C"));
                                    Log.d("SESSION MIN_GRADE_C :", session + "MIN_GRADE_C_" + obj.getString("MIN_GRADE_C"));


                                    utils.setDataSession(QuestionerActivity.this, session + "MAX_GRADE_C_" + obj.getString("MAX_GRADE_C"), obj.getString("MAX_GRADE_C"));
                                    Log.d("SESSION MAX_GRADE_C :", session + "MAX_GRADE_C_" + obj.getString("MAX_GRADE_C"));


                                    // D
                                    utils.setDataSession(QuestionerActivity.this, session + "QST_GRADE_D_" + obj.getString("QST_GRADE_D"), obj.getString("QST_GRADE_D"));
                                    Log.d("SESSION QST_GRADE_D :", session + "QST_GRADE_D_" + obj.getString("QST_GRADE_D"));


                                    utils.setDataSession(QuestionerActivity.this, session + "_MIN_GRADE_B_" + obj.getString("MIN_GRADE_D"), obj.getString("MIN_GRADE_D"));
                                    Log.d("SESSION MIN_GRADE_D :", session + "MIN_GRADE_D_" + obj.getString("MIN_GRADE_D"));


                                    utils.setDataSession(QuestionerActivity.this, session + "_MAX_GRADE_D_" + obj.getString("MAX_GRADE_D"), obj.getString("MAX_GRADE_D"));
                                    Log.d("SESSION MAX_GRADE_D :", session + "MAX_GRADE_D_" + obj.getString("MAX_GRADE_D"));


                                    System.out.println("=============================================================================================");

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
