package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONObject;

import butterknife.ButterKnife;

@SuppressWarnings("deprecation")
public class PreviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ButterKnife.bind(this);
    }



    void saveTranstHeader(String url){
        final ProgressDialog dialog = utils.showDialogBar(this,"Loading...");
        dialog.show();
        AndroidNetworking.post(url)
                .addBodyParameter("","")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    void saveTransDetails(String url){
        final ProgressDialog dialog = utils.showDialogBar(this, "Loading...");
        dialog.show();
        AndroidNetworking.post(url)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    void saveTransOptions(String url){
        final ProgressDialog dialog = utils.showDialogBar(this,"Loading...");
        AndroidNetworking.post(url)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    void savePhoto(String url){
        final ProgressDialog dialog = utils.showDialogBar(this, "Loading...");
        AndroidNetworking.post(url)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

}
