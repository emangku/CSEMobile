package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.adapter.DemoAdapter;
import com.creativeit.csemobile.gps_location.GPSTracking;
import com.creativeit.csemobile.helpers.utils;
import com.creativeit.csemobile.model.DemoModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DemoActivity extends AppCompatActivity {
    GPSTracking gps;
    double latitude;
    double longitude;

    @BindView(R.id.viewDemo)
    RecyclerView viewDemo;
    DemoAdapter adapter;
    List<DemoModel> list;
    String status = "";
    String jam = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        ButterKnife.bind(this);
        getGPS();
        AndroidNetworking.get("https://ipapi.co/8.8.8.8/json/")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getApplicationContext(), response.getString("ip"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }


    void getGPS(){
        gps = new GPSTracking(this);
        if (gps.canGetLocation()){
            latitude = gps.getLatitude();
            longitude= gps.getLongitude();
            Toast.makeText(DemoActivity.this,"Location :" +latitude+","+longitude,Toast.LENGTH_LONG).show();
        }else{
            gps.showSettingAlerts();
        }
    }
    void absen_jn(String url){
        status = "in";
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle(null);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        AndroidNetworking.post(url)
                .addBodyParameter("nik", utils.getDataSession(this,"sessionNIK"))
                .addBodyParameter("jam",jam)
                .addBodyParameter("lokasi",latitude+","+longitude)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                Intent intent = new Intent(DemoActivity.this,HomeActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(DemoActivity.this,"Erorr :" + anError.toString(),Toast.LENGTH_LONG).show();
                    }
                });
    }
}
