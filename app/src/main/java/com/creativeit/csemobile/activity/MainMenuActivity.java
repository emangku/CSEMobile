package com.creativeit.csemobile.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.helpers.utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainMenuActivity extends AppCompatActivity {
    @BindView(R.id.viewChangeStore)Button viewChangeStore;
    @BindView(R.id.viewQuestionerSurveyHistory)Button viewQuestionerSurveyHistory;
    @BindView(R.id.viewQuestionerBtnSheet)Button viewQuestionerBtnSheet;
    @BindView(R.id.viewHeaderTitle)TextView viewHeaderTitle;
    String session_store_branch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        ButterKnife.bind(this);
        session_store_branch =  utils.getDataSession(this,"sessionStore");
        viewHeaderTitle.setText(utils.getDataSession(this,"sessionvRegion")+" - "+session_store_branch);
    }


    @OnClick(R.id.viewQuestionerBtnSheet) void viewQuestionerBtnSheet(){
        startActivity(new Intent(this, QuestionerActivity.class));
    }
    @OnClick(R.id.viewQuestionerSurveyHistory) void viewQuestionerSurveyHistory(){
        startActivityForResult(new Intent(this, QuestionerSurveyHistoryActivity.class),0);
    }
    @OnClick(R.id.viewChangeStore)void viewChangeStore(){

        String  ses = "stores";
        Intent intent = new Intent(MainMenuActivity.this, HomeActivity.class);
        intent.putExtra("session_back_store",ses);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
