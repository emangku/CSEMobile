package com.creativeit.csemobile.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.fragment.LoginFrgments;
import com.creativeit.csemobile.fragment.RegionFragment;
import com.creativeit.csemobile.fragment.StoreFragment;
import com.creativeit.csemobile.helpers.utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


@SuppressLint("StaticFieldLeak")
public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.viewUserIdCode) TextView viewUserIdCode;
    @BindView(R.id.viewUserName) TextView viewUserName;
    @BindView(R.id.viewTxtLogout) TextView viewTxtLogout;
    @SuppressLint("StaticFieldLeak")
    public static TextView sviewUserIdCode;
    public static TextView sviewUserName;
    public  static  String sessionUsr, sessionUID = "";
    public String ses = "";
    FragmentManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        init();
        sessionUsr = utils.getDataSession(this,"sessionUSRMS_NAME");
        sessionUID = utils.getDataSession(this,"sessionUSERID_CODE");
        viewUserName.setText(sessionUsr);
        viewUserIdCode.setText(sessionUID);
        ses = getIntent().getStringExtra("session_back_store");
        if (ses != null && ses.equals("stores")){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame, new StoreFragment())
                    .addToBackStack("Login")
                    .commit();
        }else if (ses == null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame, new RegionFragment())
                    .addToBackStack("Login")
                    .commit();
        }

    }

    void init(){
//        sviewUserIdCode = (TextView) findViewById(R.id.viewUserIdCode);
//        sviewUserName = (TextView) findViewById(R.id.viewUserName);
        manager = getSupportFragmentManager();
    }
    @OnClick(R.id.viewTxtLogout) void viewTxtLogout(){
        utils.removeSessionData(this,"sessionUSRMS_NAME");
        utils.removeSessionData(this,"sessionUSERID_CODE");
        startActivity(new Intent(HomeActivity.this, MainActivity.class));
    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() ==1){
            new AlertDialog.Builder(this)
                    .setTitle(null)
                    .setMessage("Yakin Ingin keluar")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            dialog.cancel();
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frame, new LoginFrgments())
                            .addToBackStack("Login")
                            .commit();
                    dialog.cancel();
                }
            }).show();
        }
        super.onBackPressed();
    }
    @Nullable
    public static TextView getSviewUserIdCode(){
        if (sessionUID != null) {
            sviewUserIdCode.setText(sessionUID);
            return sviewUserIdCode;
        }
        return null;
    }
    @Nullable
    public static TextView getSviewUserName(){
        if (sessionUsr != null){
            sviewUserName.setText(sessionUsr);
            return sviewUserName;
        }
        return null;
    }



    public void onAttach(View view){

    }

    public void addLoginFragment(View view){
        LoginFrgments lf = new LoginFrgments();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.frame,lf,"login");
        transaction.commit();
    }


    public void removeAdd(){

    }

}
