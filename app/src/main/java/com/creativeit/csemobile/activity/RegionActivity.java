package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

@SuppressWarnings("deprecation")
public class RegionActivity extends AppCompatActivity {
    @BindView(R.id.viewSelectSpinner)
    Spinner viewSelectSpinner;
    @BindView(R.id.viewSelectBranch)
    Button viewSelectBranch;
    String opt = "";
    String reg = "";
    String regions = "";
    private List<String> list;
    ArrayAdapter<String> adapter;
    String[] region = {"--Select Region--", "JAK", "BALI", "BANDUNG"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region);
        ButterKnife.bind(this);
        init();
    }

    void init() {
        loadSpinerIdType();
    }

    void loadSpinerIdType() {
        loadRegion();
    }


    @OnClick(R.id.viewSelectBranch)
    void viewSelectBranch() {
        if (opt.equals("")) {
            Toast.makeText(this, "Silahkan Pilih Region", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, opt, Toast.LENGTH_LONG).show();

            if (opt.equals("BALI")) {
                reg = "BAL";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("JAKARTA")) {
                reg = "JAK";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("BANDUNG")) {
                reg = "BDG";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("SURABAYA")) {
                reg = "SUB";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("MAKASAR")) {
                reg = "MKS";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("BATAM")) {
                reg = "BAT";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("YOGYAKARTA")) {
                reg = "YOG";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("MEDAN")) {
                reg = "MDN";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
            if (opt.equals("HEAD OFFICE")) {
                reg = "HO";
                utils.setDataSession(this, "sessionvRegion", opt);
                utils.setDataSession(this, "sessionvRegCode", reg);
            }
           startActivity(new Intent(RegionActivity.this, StoreActivity.class));
        }
    }
    void loadRegion() {
        final ProgressDialog dialog = utils.showDialogBar(this, "Loading...");
        dialog.show();
        AndroidNetworking.get(getResources().getString(R.string.url_region))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                list = new ArrayList<String>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    regions = object.getString("REG_CODE");
                                    list.add(regions);
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        adapter = new ArrayAdapter<String>(RegionActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
                        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        viewSelectSpinner.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {

                        dialog.dismiss();
                    }
                });
    }
    @OnItemSelected(R.id.viewSelectSpinner)
    void viewSelectSpinner(int position) {
        opt = viewSelectSpinner.getItemAtPosition(position).toString();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
