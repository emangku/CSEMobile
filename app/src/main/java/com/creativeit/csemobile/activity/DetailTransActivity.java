package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("deprecation")
public class DetailTransActivity extends AppCompatActivity {
    @BindView(R.id.viewRegQst_details)TextView viewRegQst_details;
    String cnvrt = "";
    String cnvrt_2 = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_trans);
        ButterKnife.bind(this);
        loadHead(getResources().getString(R.string.details_trans_lvl_1));
    }

    void loadHead(String url){
        final ProgressDialog dialog = utils.showDialogBar(DetailTransActivity.this,"Loading..");
        dialog.show();
        AndroidNetworking.post(url)
                .addBodyParameter("STORE_CODE",utils.getDataSession(DetailTransActivity.this,"sessionQTR_STORE_CODE"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                JSONArray array = response.getJSONArray("data");
                                for (int i=0; i< array.length();i++){
                                    JSONObject object = array.getJSONObject(i);
                                    cnvrt = object.getString("REG");
                                    if (cnvrt.equals("BAL")){
                                        cnvrt_2 = "BALI";
                                    }
                                    if (cnvrt.equals("JAK")){
                                        cnvrt_2 = "JAKARTA";
                                    }
                                    if (cnvrt.equals("BDG")){
                                        cnvrt_2 = "BANDUNG";
                                    }
                                    if (cnvrt.equals("SUB")){
                                        cnvrt_2 = "SURABAYA";
                                    }
                                    if (cnvrt.equals("MKS")){
                                        cnvrt_2 = "MAKASAR";
                                    }
                                    if (cnvrt.equals("BAT")){
                                        cnvrt_2 = "BATAM";
                                    }
                                    if (cnvrt.equals("YOG")){
                                        cnvrt_2 = "YOGYAKARTA";
                                    }
                                    if (cnvrt.equals("MDN")){
                                        cnvrt_2 = "MEDAN";
                                    }
                                    if (cnvrt.equals("HO")){
                                        cnvrt_2 = "HEAD OFFICE";
                                    }
                                    viewRegQst_details.setText(cnvrt_2+" - "+object.getString("STORE_CODE")+" - "+object.getString("STORE_NAME"));
                                    dialog.dismiss();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                    }
                });
    }
}
