package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.adapter.HistoryAdapter;
import com.creativeit.csemobile.helpers.utils;
import com.creativeit.csemobile.model.QSTTransHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;

@SuppressWarnings("deprecation")
public class QuestionerSurveyHistoryActivity extends AppCompatActivity{
//    @BindView(R.id.viewBTNLogouts)Button viewBTNLogouts;
    @BindView(R.id.viewRefresh)SwipeRefreshLayout viewRefresh;
    @BindView(R.id.viewRcler)RecyclerView viewRcler;
    @BindView(R.id.viewOPT)Spinner viewOPT;
    ArrayAdapter<String> adapters;
    List<String> lists;
    String opt = "";
    String opt_string = "";
    String opt_convt = "";
    private List<QSTTransHistoryModel> list;
    private HistoryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questioner_survey_history);
        ButterKnife.bind(this);
        init();
    }
    void init(){
        loadOptHystory(getResources().getString(R.string.opt_history));
        viewRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataHistorySurvey(getResources().getString(R.string.history_qst),opt_convt);
                viewRefresh.setRefreshing(false);
            }
        });
    }
    void loadOptHystory(String url){
        final ProgressDialog dialog = utils.showDialogBar(this,"loading...");
        dialog.show();
        AndroidNetworking.get(url)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                                lists = new ArrayList<String>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i=0; i<array.length();i++){
                                    JSONObject object = array.getJSONObject(i);
                                    opt_string = object.getString("QST_ID") + " - " +object.getString("QST_CAPTION");
                                    System.out.println("OUTPUT : "+opt_string);
                                    lists.add(opt_string);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        adapters = new ArrayAdapter<String>(QuestionerSurveyHistoryActivity.this,R.layout.support_simple_spinner_dropdown_item,lists);
                        adapters.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        viewOPT.setAdapter(adapters);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(),"Opps Error : "+anError.getMessage(),Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }
    @OnItemSelected(R.id.viewOPT) void viewOPT(int position){
        opt = viewOPT.getItemAtPosition(position).toString();
        if (opt.equals("QS06 - Lembar Evaluasi Kepuasan Pelanggan")){
            opt_convt = "QS06";
        }
        if (opt.equals("QS05 - Lembar Evaluasi Kepuasan Pelanggan")){
            opt_convt = "QS05";
        }
        if (opt.equals("QS04 - Lembar Evaluasi Kepuasan Pelanggan")){
            opt_convt = "QS04";
        }
        if (opt.equals("QS03 - Lembar Evaluasi Kepuasan Pelanggan")){
            opt_convt = "QS03";
        }
        if (opt.equals("QS02 - Lembar Evaluasi Kepuasan Pelanggan")){
            opt_convt = "QS02";
        }
        if (opt.equals("QS01 - Lembar Evaluasi Kepuasan Pelanggan")){
            opt_convt = "QS01";
        }
        Toast.makeText(getApplicationContext(),opt_convt, Toast.LENGTH_LONG).show();
        loadDataHistorySurvey(getResources().getString(R.string.history_qst),opt_convt);
    }
    void loadDataHistorySurvey(String url, String params){
        final ProgressDialog dialog = utils.showDialogBar(this,"Loading...");
        dialog.show();
        AndroidNetworking.post(url)
                .addBodyParameter("QST_ID",params)
                .addBodyParameter("QTR_BY","300701010")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                list = new ArrayList<QSTTransHistoryModel>();
                                QSTTransHistoryModel model = new QSTTransHistoryModel();
                                JSONArray array = response.getJSONArray("data");
                                for (int i=0; i< array.length();i++){
                                    JSONObject object = array.getJSONObject(i);
                                    model.setQTR_ID(object.getString("QTR_ID"));
                                    model.setQST_ID(object.getString("QST_ID"));
                                    model.setQST_CAPTION(object.getString("QST_CAPTION"));
                                    model.setQTR_STORE_CODE(object.getString("QTR_STORE_CODE"));
                                    model.setQTR_STORE_NAME(object.getString("QTR_STORE_NAME"));
                                    model.setREG_CODE(object.getString("REG_CODE"));
                                    model.setQTR_DATE(object.getString("QTR_DATE"));
                                    model.setQTR_BY(object.getString("QTR_BY"));
                                    model.setQTR_USRMS_NAME(object.getString("QTR_USRMS_NAME"));
                                    model.setAK_BY(object.getString("AK_BY"));
                                    model.setAK_BY_NAME(object.getString("AK_BY_NAME"));
                                    model.setTOTAL(object.getString("TOTAL"));
                                    model.setQST_GRADE(object.getString("QST_GRADE"));
                                    list.add(model);
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        viewRcler.setLayoutManager(new LinearLayoutManager(QuestionerSurveyHistoryActivity.this));
                        adapter = new HistoryAdapter(QuestionerSurveyHistoryActivity.this,list);
                        viewRcler.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(),"Opps Error"+anError.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("Opps Error ",anError.getMessage());
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
