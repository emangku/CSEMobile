package com.creativeit.csemobile.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.fragment.RegionFragment;
import com.creativeit.csemobile.helpers.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

@SuppressWarnings("deprecation")
public class StoreActivity extends AppCompatActivity {
    @BindView(R.id.viewSelectedStore)Spinner viewSelectedStore;
    @BindView(R.id.viewBTNStoreSelected)Button viewBTNStoreSelected;
    @BindView(R.id.viewBackChangeBranch)Button viewBackChangeBranch;
    @BindView(R.id.viewSessRegion)TextView viewSessRegion;
    String opt = "";
    String store = "";
    ArrayAdapter<String> adapter;
    List<String> list;

    String sessionReg = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        ButterKnife.bind(this);
        viewSessRegion.setText(utils.getDataSession(this,"sessionvRegion"));
        init();

    }

    void init(){
        sessionReg = utils.getDataSession(this,"sessionvRegCode");
        LoadStoreByregion();
    }

    void LoadStoreByregion(){
        final ProgressDialog dialog = utils.showDialogBar(this,"Loading...");
        dialog.show();
        AndroidNetworking.post(getResources().getString(R.string.url_store_dest))
                .addBodyParameter("REG_CODE",sessionReg)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                list = new ArrayList<String>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    store = object.getString("BU_CODE") + " - " +object.getString("BU_NAME") ;
                                    list.add(store);
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        adapter = new ArrayAdapter<String>(StoreActivity.this,R.layout.support_simple_spinner_dropdown_item,list);
                        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        viewSelectedStore.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(StoreActivity.this,anError.getMessage(),Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }

    @OnItemSelected(R.id.viewSelectedStore) void viewSelectedStore(int position){
        opt = viewSelectedStore.getItemAtPosition(position).toString();
        utils.setDataSession(this,"sessionStore",opt);
    }

    @OnClick(R.id.viewBTNStoreSelected) void viewBTNStoreSelected(){

        if (opt.equals("")){
            Toast.makeText(this,"Silahkan Pilih", Toast.LENGTH_LONG).show();
        }else{
            startActivity(new Intent(StoreActivity.this, MainMenuActivity.class));
        }

    }
    @OnClick(R.id.viewBackChangeBranch) void viewBackChangeBranch(){
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.frame, new RegionFragment())
                .commit();
        utils.removeSessionData(this,"sessionvRegCode");
    }
}
