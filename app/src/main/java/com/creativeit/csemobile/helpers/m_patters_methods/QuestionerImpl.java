package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerModel;

import java.util.List;


public interface QuestionerImpl {
    long saveQuestioner(QuestionerModel q);
    QuestionerModel getQuestioner(long id);
    List<QuestionerModel> getAllQuestioner();
    int getQuestionerCount();
    int updateQuestioner(QuestionerModel q);
    void deleteQuestioner(QuestionerModel q);
}
