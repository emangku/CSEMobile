package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerTransDetailModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface QuestionerTransDetailImpl {
    long saveQuestionerTransDetail(QuestionerTransDetailModel q);
    QuestionerTransDetailModel getQuestionerTransDetail(long id);
    List<QuestionerTransDetailModel> getAllQuestionerTransDetail();
    int getQuestionerTransDetailCount();
    int updateQuestionerTransDetail(QuestionerTransDetailModel q);
    void deleteQuestionerTransDetail(QuestionerTransDetailModel q);
}
