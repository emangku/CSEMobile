package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerHeadModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface QuestionerHeadImpl {
    long saveQuestionerHead(QuestionerHeadModel q);
    QuestionerHeadModel getQuestionerHead(long id);
    List<QuestionerHeadModel> getAllQuestionerHead();
    int getQuestionerHeadCount();
    int updateQuestionerHead(QuestionerHeadModel q);
    void deleteQuestionerHead(QuestionerHeadModel q);
}
