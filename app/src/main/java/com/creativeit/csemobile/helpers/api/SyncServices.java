package com.creativeit.csemobile.helpers.api;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Creef on 5/14/2018.
 */

public class SyncServices extends Service {

    public static final Object sSyncAdapterLock = new Object();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
