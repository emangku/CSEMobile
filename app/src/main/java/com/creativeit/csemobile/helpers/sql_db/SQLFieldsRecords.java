package com.creativeit.csemobile.helpers.sql_db;

import android.provider.BaseColumns;

public class SQLFieldsRecords {
    public SQLFieldsRecords() {
    }

    public static class QuestionerFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER";
        public static final String COLUMN_P_QST_ID = "P_QST_ID";
        public static final String COLUMN_QST_ID = "QST_ID";
        public static final String COLUMN_QST_LVL = "QST_LVL";
        public static final String COLUMN_QST_CAPTION = "QST_CAPTION";
        public static final String COLUMN_QST_NOTE_1 = "QST_NOTE_1";
        public static final String COLUMN_QST_NOTE_2 = "QST_NOTE_2";
        public static final String COLUMN_QST_NOTE_3 = "QST_NOTE_3";
        public static final String COLUMN_QST_NOTE_4 = "QST_NOTE_4";
        public static final String COLUMN_QST_NOTE_5 = "QST_NOTE_5";
        public static final String COLUMN_QST_LIST_TYPE = "QST_LIST_TYPE";
        public static final String COLUMN_QST_POINT = "QST_POINT";
        public static final String COLUMN_STATUS    = "STATUS";
    }



    public static class QuestionerGradeFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER_GRADE";
        public static final String COLUMN_QST_ID = "QST_ID";
        public static final String COLUMN_QST_GRADE = "QST_GRADE";
        public static final String COLUMN_MIN_GRADE = "MIN_GRADE";
        public static final String COLUMN_MAX_GRADE = "MAX_GRADE";
        public static final String COLUMN_STATUS    = "STATUS";
    }


    public static class QuestionerHeadFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER_HEAD";
        public static final String COLUMN_QST_ID = "QST_ID";
        public static final String COLUMN_REG_CODE = "REG_CODE";
        public static final String COLUMN_QST_MONTH = "QST_MONTH";
        public static final String COLUMN_QST_YEAR = "QST_YEAR";
        public static final String COLUMN_QST_STATUS = "QST_STATUS";
        public static final String COLUMN_DATA_DATE = "DATA_DATE";
        public static final String COLUMN_DATA_BY = "DATA_BY";
        public static final String COLUMN_STATUS    = "STATUS";
    }


    public static class QuestionerHeadStatusFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER_HEAD_STATUS";
        public static final String COLUMN_QST_ID = "QST_ID";
        public static final String COLUMN_REG_CODE = "REG_CODE";
        public static final String COLUMN_QST_MONTH = "QST_MONTH";
        public static final String COLUMN_QST_YEAR = "QST_YEAR";
        public static final String COLUMN_QST_STATUS = "QST_STATUS";
        public static final String COLUMN_STATUS_DATE = "STATUS_DATE";
        public static final String COLUMN_STATUS_BY = "STATUS_BY";
        public static final String COLUMN_STATUS    = "STATUS";
    }


    public static class QuestionerTransDetailFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER_TRANS_DETAIL";
        public static final String COLUMN_QTR_ID = "QTR_ID";
        public static final String COLUMN_QST_ID = "QST_ID";
        public static final String COLUMN_QST_CHECK = "QST_CHECK";
        public static final String COLUMN_COMMENTS = "COMMENTS";
        public static final String COLUMN_STATUS    = "STATUS";
    }



    public static class QuestionerTransHeaderFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER_TRANS_HEADER";
        public static final String COLUMN_QTR_ID = "QTR_ID";
        public static final String COLUMN_QST_ID = "QST_ID";
        public static final String COLUMN_STORE_CODE = "STORE_CODE";
        public static final String COLUMN_QTR_DATE = "QTR_DATE";
        public static final String COLUMN_QTR_BY = "QTR_BY";
        public static final String COLUMN_STATUS    = "STATUS";
    }


    public static class QuestionerTransOptionFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER_TRANS_OPTIONS";
        public static final String COLUMN_QTR_ID = "QTR_ID";
        public static final String COLUMN_OPT_ID = "OPT_ID";
        public static final String COLUMN_OPT_CAPTION_1 = "OPT_CAPTION_1";
        public static final String COLUMN_OPT_CAPTION_2 = "OPT_CAPTION_2";
        public static final String COLUMN_IS_CHECK = "IS_CHECK";
        public static final String COLUMN_STATUS    = "STATUS";
    }

    public static class QuestionerTransPhotoFields implements BaseColumns {
        public static final String TABLE_NAME = "QUESTIONER_TRANS_PHOTO";
        public static final String COLUMN_QTR_ID = "QTR_ID";
        public static final String COLUMN_FILE_ID = "FILE_ID";
        public static final String COLUMN_FILE_LOC = "FILE_LOC";
        public static final String COLUMN_QTR_BY = "QTR_BY";
        public static final String COLUMN_STATUS    = "STATUS";
    }



    public static class UserAccessFields implements BaseColumns {
        public static final String TABLE_NAME = "USER_ACCESS";
        public static final String COLUMN_USRMS_ID = "USRMS_ID";
        public static final String COLUMN_USRMS_ST = "USRMS_ST";
        public static final String COLUMN_STATUS    = "STATUS";
    }

}
