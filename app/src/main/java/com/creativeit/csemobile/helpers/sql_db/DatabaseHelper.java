package com.creativeit.csemobile.helpers.sql_db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerGradeImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerHeadImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerHeadStatusImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerTransDetailImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerTransHeadImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerTransOptionImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.QuestionerTransPhotoImpl;
import com.creativeit.csemobile.helpers.m_patters_methods.UserAccessImpl;
import com.creativeit.csemobile.model.QuestionerGradeModel;
import com.creativeit.csemobile.model.QuestionerHeadModel;
import com.creativeit.csemobile.model.QuestionerHeadStatusModel;
import com.creativeit.csemobile.model.QuestionerModel;
import com.creativeit.csemobile.model.QuestionerTransDetailModel;
import com.creativeit.csemobile.model.QuestionerTransHeadModel;
import com.creativeit.csemobile.model.QuestionerTransOptionModel;
import com.creativeit.csemobile.model.QuestionerTransPhotoModel;
import com.creativeit.csemobile.model.UserAccessModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Creef on 4/26/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper
        implements QuestionerImpl, QuestionerGradeImpl, QuestionerHeadImpl,
        QuestionerHeadStatusImpl, QuestionerTransDetailImpl, QuestionerTransHeadImpl,
        QuestionerTransOptionImpl, QuestionerTransPhotoImpl, UserAccessImpl {
    private static final int DB_VERSION = 1;
    private static String DB_PATH = "/data/data/com.creativeit.csemobile/databases/";
    private static String DB_NAME = "cse";
    private static String Table_name = "Quiz";
    private final Context myContext;
    private SQLiteDatabase myDataBase;
    private SQLiteDatabase myData;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.myContext = context;
    }

    public void createDataBase() throws IOException {


        boolean dbExist = checkDataBase();
        if (dbExist) {
            //do nothing - database already exist
        } else {
            CopyFiles();
        }
    }

    private void CopyFiles() {
        try {
            InputStream is = myContext.getAssets().open(DB_NAME);
            File outfile = new File(DB_PATH, DB_NAME);
            outfile.getParentFile().mkdirs();
            outfile.createNewFile();


            if (is == null)
                throw new RuntimeException("stream is null");
            else {
                FileOutputStream out = new FileOutputStream(outfile);
                // BufferedOutputStream out = new BufferedOutputStream( new FileOutputStream(outfile));
                byte buf[] = new byte[128];
                do {
                    int numread = is.read(buf);
                    if (numread <= 0) break;
                    out.write(buf, 0, numread);
                } while (true);
                is.close();
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {

        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QuestionerModel.SQL_CREATE_QUESTIONER);
        db.execSQL(QuestionerGradeModel.SQL_CREATE_QUESTIONER_GRADE);
        db.execSQL(QuestionerHeadModel.SQL_CREATE_QUESTIONER_HEAD);
        db.execSQL(QuestionerHeadStatusModel.SQL_CREATE_QUESTIONER_HEAD_STATUS);
        db.execSQL(QuestionerTransDetailModel.SQL_CREATE_QUESTIONER_TRANS_DETAIL);
        db.execSQL(QuestionerTransHeadModel.SQL_CREATE_QUESTIONER_TRANS_HEADER);
        db.execSQL(QuestionerTransOptionModel.SQL_CREATE_QUESTIONER_TRANS_OPTIONS);
        db.execSQL(QuestionerTransPhotoModel.SQL_CREATE_QUESTIONER_TRANS_PHOTO);
        db.execSQL(UserAccessModel.SQL_CREATE_USER_ACCESS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(QuestionerModel.SQL_DELETE_QUESTIONER);
        db.execSQL(QuestionerGradeModel.SQL_DELETE_QUESTIONER_GRADE);
        db.execSQL(QuestionerHeadModel.SQL_DELETE_QUESTIONER_HEAD);
        db.execSQL(QuestionerHeadStatusModel.SQL_DELETE_QUESTIONER_HEAD_STATUS);
        db.execSQL(QuestionerTransDetailModel.SQL_DELETE_QUESTIONER_TRANS_DETAIL);
        db.execSQL(QuestionerTransHeadModel.SQL_DELETE_QUESTIONER_TRANS_HEADER);
        db.execSQL(QuestionerTransOptionModel.SQL_DELETE_QUESTIONER_TRANS_OPTIONS);
        db.execSQL(QuestionerTransPhotoModel.SQL_DELETE_QUESTIONER_TRANS_PHOTO);
        db.execSQL(UserAccessModel.SQL_DELETE_USER_ACCESS);
        onCreate(db);
    }


    public Cursor getQuiz_Content(int bookId) {
        String myPath = DB_PATH + DB_NAME;
        myData = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);


        Cursor cur;
        cur = myData.rawQuery("select quiz_text from Quiz where quiz_id='" + bookId + "'", null);
        cur.moveToFirst();


        myData.close();
        return cur;
    }

    public Cursor getQuiz_List() {
        String myPath = DB_PATH + DB_NAME;
        myData = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        int i;
        Cursor cur;
        cur = myData.rawQuery("select quiz_id,quiz_text,correct_answer from quiz", null);
        cur.moveToFirst();
        i = cur.getCount();
        myData.close();
        return cur;
    }

    public Cursor getAns(int quizid) {
        String myPath = DB_PATH + DB_NAME;
        myData = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);


        Cursor cur;
        cur = myData.rawQuery("select Answer from Answers where quiz_id='" + quizid + "'", null);
        cur.moveToFirst();
        myData.close();
        return cur;
    }

    public Cursor getAnsList() {
        String myPath = DB_PATH + DB_NAME;
        myData = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);


        Cursor cur;
        cur = myData.rawQuery("select Answer from Answers", null);
        cur.moveToFirst();
        myData.close();


        return cur;
    }


    public Cursor getCorrAns() {
        String myPath = DB_PATH + DB_NAME;
        myData = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);


        Cursor cur;
        cur = myData.rawQuery("select Correct_Answer from Quiz", null);
        cur.moveToFirst();
        myData.close();


        return cur;
    }

    @Override
    public long saveQuestioner(QuestionerModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID, q.getP_QST_ID());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID, q.getP_QST_ID());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL, q.getQST_LVL());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION, q.getQST_CAPTION());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1, q.getQST_NOTE_1());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2, q.getQST_NOTE_2());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3, q.getQST_NOTE_3());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4, q.getQST_NOTE_4());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5, q.getQST_NOTE_5());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE, q.getQST_LIST_TYPE());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT, q.getQST_POINT());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.QuestionerFields.TABLE_NAME, null, values);
        db.close();

        return id;
    }


    public boolean addQuestioner(String P_QST_ID,
                                 String QST_ID,
                                 String QST_LVL,
                                 String QST_CAPTION,
                                 String QST_NOTE_1,
                                 String QST_NOTE_2,
                                 String QST_NOTE_3,
                                 String QST_NOTE_4,
                                 String QST_NOTE_5,
                                 String QST_LIST_TYPE,
                                 String QST_POINT,
                                 String STATUS
    ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID, P_QST_ID);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID, QST_ID);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL, QST_LVL);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION, QST_CAPTION);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1, QST_NOTE_1);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2, QST_NOTE_2);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3, QST_NOTE_3);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4, QST_NOTE_4);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5, QST_NOTE_5);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE, QST_LIST_TYPE);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT, QST_POINT);
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS, STATUS);

        db.insert(SQLFieldsRecords.QuestionerFields.TABLE_NAME, null, values);
        db.close();

        return true;
    }

    public boolean updateQuestioner(String STATUS) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS, STATUS);
        db.update(SQLFieldsRecords.QuestionerFields.TABLE_NAME, values, SQLFieldsRecords.QuestionerFields.COLUMN_STATUS + "=" + STATUS, null);

        return true;
    }


    public Cursor getQuestioner() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME + " ORDER BY " + SQLFieldsRecords.QuestionerFields._ID + " ASC";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public Cursor UnSyncedQuestioner() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME + " WHERE " + SQLFieldsRecords.QuestionerFields.COLUMN_STATUS + "=0;";
        Cursor cursor = db.rawQuery(sql, null);

        return cursor;
    }

    @Override
    public QuestionerModel getQuestioner(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE,
                        SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT,
                        SQLFieldsRecords.QuestionerFields.COLUMN_STATUS},
                SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerModel qm = new QuestionerModel(
                cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields._ID)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS)));
        cursor.close();

        return qm;
    }

    public Cursor AsncUestioner() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME + " WHERE " + SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT + " = 0;";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public ArrayList<HashMap<String, String>> getVAllQuestioner() {
        ArrayList<HashMap<String, String>> list;
        list = new ArrayList<HashMap<String, String>>();
        String sql = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME
                + " WHERE " + SQLFieldsRecords.QuestionerFields.COLUMN_STATUS + "=0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID, cursor.getString(0));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID, cursor.getString(1));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL, cursor.getString(2));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION, cursor.getString(3));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1, cursor.getString(4));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2, cursor.getString(5));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3, cursor.getString(6));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4, cursor.getString(7));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5, cursor.getString(8));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE, cursor.getString(9));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT, cursor.getString(10));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS, cursor.getString(11));
                list.add(map);
            } while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public String compresJSONFromSQLite() {
        ArrayList<HashMap<String, String>> list;
        list = new ArrayList<HashMap<String, String>>();
        String sql = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME
                + " WHERE " + SQLFieldsRecords.QuestionerFields.COLUMN_STATUS + "=0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID, cursor.getString(0));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID, cursor.getString(1));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL, cursor.getString(2));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION, cursor.getString(3));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1, cursor.getString(4));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2, cursor.getString(5));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3, cursor.getString(6));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4, cursor.getString(7));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5, cursor.getString(8));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE, cursor.getString(9));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT, cursor.getString(10));
                map.put(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS, cursor.getString(11));
                list.add(map);
            } while (cursor.moveToNext());
        }
        db.close();
        Gson gson = new GsonBuilder().create();
        return gson.toJson(list);
    }


    public String getSyncStatus() {
        String msg = "";
        if (this.dbSyncCount() == 0) {
            msg = "SQLite and Remote MySQL DBs are in Sync!";
        } else {
            msg = "DB Sync needed\n";
        }
        return msg;
    }

    public int dbSyncCount() {
        int count = 0;
        String sql = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME
                + " WHERE " + SQLFieldsRecords.QuestionerFields.COLUMN_STATUS + "=0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        count = cursor.getCount();
        db.close();
        return count;
    }

    public void updateSyncStatus(int id, String status) {
        SQLiteDatabase database = this.getWritableDatabase();
        String updateQuery = "UPDATE " + SQLFieldsRecords.QuestionerFields.TABLE_NAME
                + " SET STATUS = '" + status + "' WHERE " + SQLFieldsRecords.QuestionerFields._ID + "=" + "'" + id + "'";
        Log.d("query", updateQuery);
        database.execSQL(updateQuery);
        database.close();
    }

    @Override
    public List<QuestionerModel> getAllQuestioner() {
        List<QuestionerModel> ls = new ArrayList<>();
        String sqlqcrQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sqlqcrQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionerModel qms = new QuestionerModel();
                qms.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields._ID)));
                qms.setP_QST_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID)));
                qms.setQST_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID)));
                qms.setQST_LVL(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL)));
                qms.setQST_CAPTION(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION)));
                qms.setQST_NOTE_1(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1)));
                qms.setQST_NOTE_2(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2)));
                qms.setQST_NOTE_3(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3)));
                qms.setQST_NOTE_4(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4)));
                qms.setQST_NOTE_5(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5)));
                qms.setQST_LIST_TYPE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE)));
                qms.setQST_POINT(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT)));
                qms.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS)));
                ls.add(qms);
            } while (cursor.moveToNext());
        }
        return ls;
    }

    @Override
    public int getQuestionerCount() {
        String sQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sQuery, null);
        int count = cursor.getCount();
        return count;
    }

    @Override
    public int updateQuestioner(QuestionerModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID, q.getP_QST_ID());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL, q.getQST_LVL());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION, q.getQST_CAPTION());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1, q.getQST_NOTE_1());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2, q.getQST_NOTE_2());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3, q.getQST_NOTE_3());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4, q.getQST_NOTE_4());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5, q.getQST_NOTE_5());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE, q.getQST_LIST_TYPE());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT, q.getQST_POINT());
        values.put(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS, q.getSTATUS());

        return db.update(SQLFieldsRecords.QuestionerFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID + "=?",
                new String[]{String.valueOf(q.getP_QST_ID())});
    }

    @Override
    public void deleteQuestioner(QuestionerModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerFields.TABLE_NAME, SQLFieldsRecords.QuestionerFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }


    @Override
    public long saveQuestionerGrade(QuestionerGradeModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_GRADE, q.getQST_GRADE());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MIN_GRADE, q.getMIN_GRADE());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MAX_GRADE, q.getMAX_GRADE());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME, null, values);

        db.close();
        return id;
    }

    @Override
    public QuestionerGradeModel getQuestionerGrade(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID,
                        SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_GRADE,
                        SQLFieldsRecords.QuestionerGradeFields.COLUMN_MAX_GRADE,
                        SQLFieldsRecords.QuestionerGradeFields.COLUMN_STATUS},
                SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerGradeModel qgm = new QuestionerGradeModel(
                cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields._ID)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_GRADE)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MIN_GRADE)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MAX_GRADE)),
                cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_STATUS)));
        cursor.close();
        return qgm;
    }

    @Override
    public List<QuestionerGradeModel> getAllQuestionerGrade() {
        List<QuestionerGradeModel> list = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerGradeFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionerGradeModel qg = new QuestionerGradeModel();
                qg.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields._ID)));
                qg.setQST_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID)));
                qg.setQST_GRADE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_GRADE)));
                qg.setMIN_GRADE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MIN_GRADE)));
                qg.setMAX_GRADE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MAX_GRADE)));
                qg.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerGradeFields.COLUMN_STATUS)));
                list.add(qg);
            } while (cursor.moveToNext());
        }
        return list;
    }

    @Override
    public int getQuestionerGradeCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        int count = cursor.getCount();
        return count;
    }

    @Override
    public int updateQuestionerGrade(QuestionerGradeModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_GRADE, q.getQST_GRADE());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MIN_GRADE, q.getMIN_GRADE());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_MAX_GRADE, q.getMAX_GRADE());
        values.put(SQLFieldsRecords.QuestionerGradeFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID + "=?",
                new String[]{String.valueOf(q.getQST_ID())});
    }

    @Override
    public void deleteQuestionerGrade(QuestionerGradeModel q) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME, SQLFieldsRecords.QuestionerGradeFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();

    }

    @Override
    public long saveQuestionerHead(QuestionerHeadModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_REG_CODE, q.getREG_CODE());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_MONTH, q.getQST_MONTH());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_YEAR, q.getQST_YEAR());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_STATUS, q.getQST_STATUS());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_DATE, q.getDATA_DATE());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_BY, q.getDATA_BY());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME, null, values);
        return id;
    }

    @Override
    public QuestionerHeadModel getQuestionerHead(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID,
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_REG_CODE,
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_MONTH,
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_YEAR,
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_STATUS,
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_DATE,
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_BY,
                        SQLFieldsRecords.QuestionerHeadFields.COLUMN_STATUS
                }, SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerHeadModel vModel =
                new QuestionerHeadModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_REG_CODE)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_MONTH)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_YEAR)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_STATUS)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_DATE)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_BY)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_STATUS))
                );
        db.close();
        return vModel;
    }

    @Override
    public List<QuestionerHeadModel> getAllQuestionerHead() {
        List<QuestionerHeadModel> lists = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerHeadFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionerHeadModel model = new QuestionerHeadModel();
                model.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields._ID)));
                model.setQST_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID)));
                model.setREG_CODE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_REG_CODE)));
                model.setQST_MONTH(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_MONTH)));
                model.setQST_YEAR(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_YEAR)));
                model.setQST_STATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_STATUS)));
                model.setDATA_DATE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_DATE)));
                model.setDATA_BY(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_BY)));
                model.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadFields.COLUMN_STATUS)));
                lists.add(model);
            } while (cursor.moveToNext());
        }
        return lists;
    }

    @Override
    public int getQuestionerHeadCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        return cursor.getCount();
    }

    @Override
    public int updateQuestionerHead(QuestionerHeadModel q) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_REG_CODE, q.getREG_CODE());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_MONTH, q.getQST_MONTH());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_YEAR, q.getQST_YEAR());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_STATUS, q.getQST_STATUS());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_DATE, q.getDATA_DATE());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_BY, q.getDATA_BY());
        values.put(SQLFieldsRecords.QuestionerHeadFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID + "=?",
                new String[]{q.getQST_ID()});
    }

    @Override
    public void deleteQuestionerHead(QuestionerHeadModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME, SQLFieldsRecords.QuestionerHeadFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }

    @Override
    public long saveQuestionerHeadStatus(QuestionerHeadStatusModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_REG_CODE, q.getREG_CODE());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_MONTH, q.getQST_MONTH());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_YEAR, q.getQST_YEAR());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_STATUS, q.getQST_STATUS());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_DATE, q.getSTATUS_DATE());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_BY, q.getSTATUS_BY());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME, null, values);
        return id;
    }

    @Override
    public QuestionerHeadStatusModel getQuestionerHeadStatus(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID,
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_REG_CODE,
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_MONTH,
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_YEAR,
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_STATUS,
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_DATE,
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_BY,
                        SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS
                }, SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerHeadStatusModel models =
                new QuestionerHeadStatusModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_REG_CODE)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_MONTH)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_YEAR)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_STATUS)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_DATE)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_BY)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS))
                );
        db.close();
        return models;
    }

    @Override
    public List<QuestionerHeadStatusModel> getAllQuestionerHeadStatus() {
        List<QuestionerHeadStatusModel> lists = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerHeadStatusFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionerHeadStatusModel model = new QuestionerHeadStatusModel();
                model.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields._ID)));
                model.setQST_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID)));
                model.setREG_CODE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_REG_CODE)));
                model.setQST_MONTH(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_MONTH)));
                model.setQST_YEAR(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_YEAR)));
                model.setQST_STATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_STATUS)));
                model.setSTATUS_DATE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_DATE)));
                model.setSTATUS_BY(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_BY)));
                model.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS)));
                lists.add(model);
            } while (cursor.moveToNext());
        }
        return lists;
    }

    @Override
    public int getQuestionerHeadStatusCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        return cursor.getCount();
    }

    @Override
    public int updateQuestionerHeadStatus(QuestionerHeadStatusModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_REG_CODE, q.getREG_CODE());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_MONTH, q.getQST_MONTH());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_YEAR, q.getQST_YEAR());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_STATUS, q.getQST_STATUS());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_DATE, q.getSTATUS_DATE());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_BY, q.getSTATUS_BY());
        values.put(SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID + "=?",
                new String[]{q.getQST_ID()});
    }

    @Override
    public void deleteQuestionerHeadStatus(QuestionerHeadStatusModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME, SQLFieldsRecords.QuestionerHeadStatusFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }

    @Override
    public long saveQuestionerTransDetail(QuestionerTransDetailModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_CHECK, q.getQST_CHECK());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_COMMENTS, q.getCOMMENTS());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME, null, values);
        return id;
    }

    @Override
    public QuestionerTransDetailModel getQuestionerTransDetail(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QTR_ID,
                        SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID,
                        SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_CHECK,
                        SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_COMMENTS,
                        SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_STATUS
                }, SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QTR_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerTransDetailModel models =
                new QuestionerTransDetailModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QTR_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_CHECK)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_COMMENTS)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_STATUS))
                );
        db.close();
        return models;
    }

    @Override
    public List<QuestionerTransDetailModel> getAllQuestionerTransDetail() {
        List<QuestionerTransDetailModel> lists = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerTransDetailFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionerTransDetailModel model = new QuestionerTransDetailModel();
                model.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields._ID)));
                model.setQTR_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QTR_ID)));
                model.setQST_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID)));
                model.setQST_CHECK(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_CHECK)));
                model.setCOMMENTS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_COMMENTS)));
                model.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_STATUS)));
                lists.add(model);
            } while (cursor.moveToNext());
        }
        return lists;
    }

    @Override
    public int getQuestionerTransDetailCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        return cursor.getCount();
    }

    @Override
    public int updateQuestionerTransDetail(QuestionerTransDetailModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_CHECK, q.getQST_CHECK());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_COMMENTS, q.getCOMMENTS());
        values.put(SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID + "=?",
                new String[]{q.getQTR_ID()});
    }

    @Override
    public void deleteQuestionerTransDetail(QuestionerTransDetailModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME, SQLFieldsRecords.QuestionerTransDetailFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }

    @Override
    public long saveQuestionerTransHead(QuestionerTransHeadModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STORE_CODE, q.getSTORE_CODE());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_DATE, q.getQTR_DATE());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_BY, q.getQTR_BY());

        long id = db.insert(SQLFieldsRecords.QuestionerTransHeaderFields.TABLE_NAME, null, values);
        return id;
    }

    @Override
    public QuestionerTransHeadModel getQuestionerTransHead(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerTransHeaderFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_ID,
                        SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QST_ID,
                        SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STORE_CODE,
                        SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_DATE,
                        SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_BY,
                }, SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerTransHeadModel models =
                new QuestionerTransHeadModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QST_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STORE_CODE)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_DATE)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_BY)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STATUS))
                );
        db.close();
        return models;
    }

    @Override
    public List<QuestionerTransHeadModel> getAllQuestionerTransHead() {
        List<QuestionerTransHeadModel> lists = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransHeaderFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerTransHeaderFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionerTransHeadModel model = new QuestionerTransHeadModel();
                model.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields._ID)));
                model.setQTR_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_ID)));
                model.setQST_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QST_ID)));
                model.setSTORE_CODE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STORE_CODE)));
                model.setQTR_DATE(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_DATE)));
                model.setQTR_BY(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_BY)));
                model.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STATUS)));
                lists.add(model);
            } while (cursor.moveToNext());
        }
        return lists;
    }

    @Override
    public int getQuestionerTransHeadCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransHeaderFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        return cursor.getCount();
    }

    @Override
    public int updateQuestionerTransHead(QuestionerTransHeadModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QST_ID, q.getQST_ID());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STORE_CODE, q.getSTORE_CODE());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_DATE, q.getQTR_DATE());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_BY, q.getQTR_BY());
        values.put(SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID + "=?",
                new String[]{q.getQTR_ID()});
    }

    @Override
    public void deleteQuestionerTransHead(QuestionerTransHeadModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerTransHeaderFields.TABLE_NAME,
                SQLFieldsRecords.QuestionerTransHeaderFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }

    @Override
    public long saveQuestionerTransOption(QuestionerTransOptionModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_ID, q.getOPT_ID());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_1, q.getOPT_CAPTION_1());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_2, q.getOPT_CAPTION_2());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_IS_CHECK, q.getIS_CHECK());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME, null, values);
        return id;
    }

    @Override
    public QuestionerTransOptionModel getQuestionerTransOption(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID,
                        SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_ID,
                        SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_1,
                        SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_2,
                        SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_IS_CHECK,
                        SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_STATUS
                }, SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerTransOptionModel models =
                new QuestionerTransOptionModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_1)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_2)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_STATUS)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_IS_CHECK))
                );
        db.close();
        return models;
    }

    @Override
    public List<QuestionerTransOptionModel> getAllQuestionerTransOption() {
        List<QuestionerTransOptionModel> lists = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerTransOptionFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionerTransOptionModel model = new QuestionerTransOptionModel();
                model.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields._ID)));
                model.setQTR_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID)));
                model.setOPT_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_ID)));
                model.setOPT_CAPTION_1(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_1)));
                model.setOPT_CAPTION_2(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_2)));
                model.setIS_CHECK(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_IS_CHECK)));
                model.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_STATUS)));
                lists.add(model);
            } while (cursor.moveToNext());
        }
        return lists;
    }

    @Override
    public int getQuestionerTransOptionCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        return cursor.getCount();
    }

    @Override
    public int updateQuestionerTransOption(QuestionerTransOptionModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_ID, q.getOPT_ID());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_1, q.getOPT_CAPTION_1());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_2, q.getOPT_CAPTION_2());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_IS_CHECK, q.getIS_CHECK());
        values.put(SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID + "=?",
                new String[]{q.getQTR_ID()});
    }

    @Override
    public void deleteQuestionerTransOption(QuestionerTransOptionModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME, SQLFieldsRecords.QuestionerTransOptionFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }

    @Override
    public long saveQuestionerTransPhoto(QuestionerTransPhotoModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_ID, q.getFILE_ID());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_LOC, q.getFILE_LOC());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_BY, q.getQTR_BY());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.QuestionerTransPhotoFields.TABLE_NAME, null, values);
        return id;
    }

    @Override
    public QuestionerTransPhotoModel getQuestionerTransPhoto(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.QuestionerTransPhotoFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID,
                        SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_ID,
                        SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_LOC,
                        SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_BY,
                        SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_STATUS
                }, SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        QuestionerTransPhotoModel models =
                new QuestionerTransPhotoModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_LOC)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_BY)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_STATUS))
                );
        db.close();
        return models;
    }

    @Override
    public List<QuestionerTransPhotoModel> getAllQuestionerTransPhoto() {
        List<QuestionerTransPhotoModel> lists = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransPhotoFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.QuestionerTransPhotoFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                QuestionerTransPhotoModel model = new QuestionerTransPhotoModel();
                model.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields._ID)));
                model.setQTR_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID)));
                model.setFILE_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_ID)));
                model.setFILE_LOC(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_LOC)));
                model.setQTR_BY(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_BY)));
                model.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_STATUS)));
                lists.add(model);
            } while (cursor.moveToNext());
        }
        return lists;
    }

    @Override
    public int getQuestionerTransPhotoCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.QuestionerTransPhotoFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        return cursor.getCount();
    }

    @Override
    public int updateQuestionerTransPhoto(QuestionerTransPhotoModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID, q.getQTR_ID());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_ID, q.getFILE_ID());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_LOC, q.getFILE_LOC());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_BY, q.getQTR_BY());
        values.put(SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.QuestionerTransPhotoFields.TABLE_NAME, values,
                SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID + "=?",
                new String[]{q.getQTR_ID()});
    }

    @Override
    public void deleteQuestionerTransPhoto(QuestionerTransPhotoModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.QuestionerTransPhotoFields.TABLE_NAME, SQLFieldsRecords.QuestionerTransPhotoFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }

    @Override
    public long saveUserAccess(UserAccessModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID, q.getUSRMS_ID());
        values.put(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ST, q.getUSRMS_ST());
        values.put(SQLFieldsRecords.UserAccessFields.COLUMN_STATUS, q.getSTATUS());

        long id = db.insert(SQLFieldsRecords.UserAccessFields.TABLE_NAME, null, values);
        return id;
    }

    @Override
    public UserAccessModel getUserAccess(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(SQLFieldsRecords.UserAccessFields.TABLE_NAME,
                new String[]{
                        SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID,
                        SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ST,
                        SQLFieldsRecords.UserAccessFields.COLUMN_STATUS
                }, SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        UserAccessModel models =
                new UserAccessModel(
                        cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields._ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ST)),
                        cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields.COLUMN_STATUS))
                );
        db.close();
        return models;
    }

    @Override
    public List<UserAccessModel> getAllUserAccess() {
        List<UserAccessModel> lists = new ArrayList<>();
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.UserAccessFields.TABLE_NAME
                + " ORDER BY " + SQLFieldsRecords.UserAccessFields._ID + " DESC ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sqlQuery, null);

        if (cursor.moveToFirst()) {
            do {
                UserAccessModel model = new UserAccessModel();
                model.set_ID(cursor.getInt(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields._ID)));
                model.setUSRMS_ID(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID)));
                model.setUSRMS_ST(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ST)));
                model.setSTATUS(cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.UserAccessFields.COLUMN_STATUS)));
                lists.add(model);
            } while (cursor.moveToNext());
        }
        return lists;
    }

    @Override
    public int getUserAccessCount() {
        String sqlQuery = "SELECT * FROM " + SQLFieldsRecords.UserAccessFields.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        return cursor.getCount();
    }

    @Override
    public int updateUserAccess(UserAccessModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID, q.getUSRMS_ID());
        values.put(SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ST, q.getUSRMS_ST());
        values.put(SQLFieldsRecords.UserAccessFields.COLUMN_STATUS, q.getSTATUS());
        return db.update(SQLFieldsRecords.UserAccessFields.TABLE_NAME, values,
                SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID + "=?",
                new String[]{q.getUSRMS_ID()});
    }

    @Override
    public void deleteUserAccess(UserAccessModel q) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SQLFieldsRecords.UserAccessFields.TABLE_NAME, SQLFieldsRecords.UserAccessFields._ID + "=?",
                new String[]{String.valueOf(q.get_ID())});
        db.close();
    }
}
