package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.UserAccessModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface UserAccessImpl {
    long saveUserAccess(UserAccessModel q);
    UserAccessModel getUserAccess(long id);
    List<UserAccessModel> getAllUserAccess();
    int getUserAccessCount();
    int updateUserAccess(UserAccessModel q);
    void deleteUserAccess(UserAccessModel q);
}
