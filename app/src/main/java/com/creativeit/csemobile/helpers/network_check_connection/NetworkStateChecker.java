package com.creativeit.csemobile.helpers.network_check_connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.creativeit.csemobile.R;
import com.creativeit.csemobile.activity.ListDataActivity;
import com.creativeit.csemobile.helpers.sql_db.DatabaseHelper;
import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;
import com.creativeit.csemobile.helpers.sql_db.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NetworkStateChecker extends BroadcastReceiver {
    private Context context;
    private DatabaseHelper db;

    @Override
    public void onReceive(final Context context, Intent intent) {
        this.context = context;
        db = new DatabaseHelper(context);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                Cursor cursor = db.AsncUestioner();
                if (cursor.moveToFirst()) {
                    do {
                       saveQuestioners(
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT)),
                               cursor.getString(cursor.getColumnIndex(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS))
                       );
                    } while (cursor.moveToNext());
                }
            }
        }
    }

    public void saveQuestioners(final String P_QST_ID,
                                final String QST_ID,
                                final String QST_LVL,
                                final String QST_CAPTION,
                                final String QST_NOTE_1,
                                final String QST_NOTE_2,
                                final String QST_NOTE_3,
                                final String QST_NOTE_4,
                                final String QST_NOTE_5,
                                final String QST_LIST_TYPE,
                                final String QST_POINT,
                                final String STATUS){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                context.getResources().getString(R.string.url_qst),
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (!obj.getBoolean("error")) {
                        db.updateQuestioner(ListDataActivity.NAME_SYNCED_WITH_SERVER);
                        context.sendBroadcast(new Intent(ListDataActivity.DATA_SAVED_BROADCAST));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERROR :  ? ? ? ? "+error.getMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID,P_QST_ID);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID,QST_ID);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL,QST_LVL);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION,QST_CAPTION);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1,QST_NOTE_1);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2,QST_NOTE_2);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3,QST_NOTE_3);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4,QST_NOTE_4);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5,QST_NOTE_5);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE,QST_LIST_TYPE);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT,QST_POINT);
                params.put(SQLFieldsRecords.QuestionerFields.COLUMN_STATUS,STATUS);
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
    public boolean checkNetworkConnection(Context m){
        this.context = m;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo !=null && networkInfo.isConnected());
    }
}
