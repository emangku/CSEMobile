package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerTransPhotoModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface QuestionerTransPhotoImpl {
    long saveQuestionerTransPhoto(QuestionerTransPhotoModel q);
    QuestionerTransPhotoModel getQuestionerTransPhoto(long id);
    List<QuestionerTransPhotoModel> getAllQuestionerTransPhoto();
    int getQuestionerTransPhotoCount();
    int updateQuestionerTransPhoto(QuestionerTransPhotoModel q);
    void deleteQuestionerTransPhoto(QuestionerTransPhotoModel q);
}
