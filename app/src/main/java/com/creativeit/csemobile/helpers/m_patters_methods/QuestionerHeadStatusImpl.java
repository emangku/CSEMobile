package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerHeadStatusModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface QuestionerHeadStatusImpl {
    long saveQuestionerHeadStatus(QuestionerHeadStatusModel q);
    QuestionerHeadStatusModel getQuestionerHeadStatus(long id);
    List<QuestionerHeadStatusModel> getAllQuestionerHeadStatus();
    int getQuestionerHeadStatusCount();
    int updateQuestionerHeadStatus(QuestionerHeadStatusModel q);
    void deleteQuestionerHeadStatus(QuestionerHeadStatusModel q);
}
