package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerTransOptionModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface QuestionerTransOptionImpl {
    long saveQuestionerTransOption(QuestionerTransOptionModel q);
    QuestionerTransOptionModel getQuestionerTransOption(long id);
    List<QuestionerTransOptionModel> getAllQuestionerTransOption();
    int getQuestionerTransOptionCount();
    int updateQuestionerTransOption(QuestionerTransOptionModel q);
    void deleteQuestionerTransOption(QuestionerTransOptionModel q);
}
