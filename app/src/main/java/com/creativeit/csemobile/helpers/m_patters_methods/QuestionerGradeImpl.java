package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerGradeModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface QuestionerGradeImpl {
    long saveQuestionerGrade(QuestionerGradeModel q);
    QuestionerGradeModel getQuestionerGrade(long id);
    List<QuestionerGradeModel> getAllQuestionerGrade();
    int getQuestionerGradeCount();
    int updateQuestionerGrade(QuestionerGradeModel q);
    void deleteQuestionerGrade(QuestionerGradeModel q);
}
