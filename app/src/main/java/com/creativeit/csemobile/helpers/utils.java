package com.creativeit.csemobile.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Creef on 4/26/2018.
 */

@SuppressWarnings("deprecation")
public class utils {
    private static SharedPreferences sharedPreferences;
    public static ProgressDialog showDialogBar(Context context, String msg){
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(msg);
        dialog.setTitle(null);
        dialog.setCancelable(false);
        return dialog;
    }

    public static void setDataSession(Context context, String key, String value){
        sharedPreferences = context.getSharedPreferences("Eko_CreativeIT_email_aja_ke_sini_ekowdd89_add_gmail_dot_com",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public static String getDataSession(Context context, String key){
        if (sharedPreferences != null){
            return sharedPreferences.getString(key, null);
        }else if (sharedPreferences == null){
            sharedPreferences = context.getSharedPreferences("Eko_CreativeIT_email_aja_ke_sini_ekowdd89_add_gmail_dot_com", Context.MODE_PRIVATE);
            return sharedPreferences.getString(key,null);
        }
        return "";
    }

    public static SharedPreferences removeSessionData(Context context, String key_session){
        sharedPreferences = context.getSharedPreferences("Eko_CreativeIT_email_aja_ke_sini_ekowdd89_add_gmail_dot_com",Context.MODE_PRIVATE);
         sharedPreferences.edit().remove(key_session).apply();
        return sharedPreferences;
    }
}
