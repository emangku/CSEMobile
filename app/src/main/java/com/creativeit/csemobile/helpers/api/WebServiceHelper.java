package com.creativeit.csemobile.helpers.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.TrustManagerFactory;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Creef on 5/3/2018.
 */

@SuppressWarnings({"deprecation", "ConstantConditions", "RedundantStringToString"})
public class WebServiceHelper {
    private static OkHttpClient client;

    public static void initialize(){
        if (client == null){
            client = new OkHttpClient();
            client.newBuilder().connectTimeout(60 * 1000, TimeUnit.SECONDS);
            client.newBuilder().readTimeout(60 * 1000, TimeUnit.SECONDS);
            client.newBuilder().writeTimeout(60 * 1000, TimeUnit.SECONDS);
            client.newBuilder().connectTimeout(800, TimeUnit.SECONDS);
        }
    }

    public  static OkHttpClient getSSLClient(){
        if (client == null){
            client = new OkHttpClient();
            client.newBuilder().connectTimeout(60 * 1000, TimeUnit.SECONDS);
            client.newBuilder().readTimeout(60 * 1000, TimeUnit.SECONDS);
            client.newBuilder().writeTimeout(60 * 1000, TimeUnit.SECONDS);
            return client;
        }
        return null;
    }

    public static String doPOST(String url, RequestBody body, String token)throws Exception{
        Headers headers = new Headers.Builder()
                .add("Authorization","Bearer "+token)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();

        return response.body().string();
    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {

            return true;
        } else {
            return false;
        }
    }

}
