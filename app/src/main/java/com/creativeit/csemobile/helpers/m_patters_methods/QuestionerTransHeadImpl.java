package com.creativeit.csemobile.helpers.m_patters_methods;

import com.creativeit.csemobile.model.QuestionerTransHeadModel;

import java.util.List;

/**
 * Created by Creef on 4/27/2018.
 */

public interface QuestionerTransHeadImpl {
    long saveQuestionerTransHead(QuestionerTransHeadModel q);
    QuestionerTransHeadModel getQuestionerTransHead(long id);
    List<QuestionerTransHeadModel> getAllQuestionerTransHead();
    int getQuestionerTransHeadCount();
    int updateQuestionerTransHead(QuestionerTransHeadModel q);
    void deleteQuestionerTransHead(QuestionerTransHeadModel q);
}
