package com.creativeit.csemobile;

import android.app.Application;

import com.creativeit.csemobile.helpers.api.WebServiceHelper;
import com.creativeit.csemobile.helpers.ssl.SSLVSoket;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;


public class Apps extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        WebServiceHelper.initialize();
        SSLVSoket.getSslServerSocketFactory();
        SSLVSoket.getUnsafeOkHttpClient().build();
    }
}
