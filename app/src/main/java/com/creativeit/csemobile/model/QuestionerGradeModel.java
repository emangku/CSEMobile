package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerGradeModel {
    private int _ID;
    private String QST_ID, QST_GRADE, MIN_GRADE, MAX_GRADE, STATUS;

    public static final String SQL_CREATE_QUESTIONER_GRADE = "CREATE TABLE "
            + SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME + " ("
            + SQLFieldsRecords.QuestionerGradeFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerGradeFields.COLUMN_QST_GRADE + " TEXT,"
            + SQLFieldsRecords.QuestionerGradeFields.COLUMN_MIN_GRADE + " INTEGER,"
            + SQLFieldsRecords.QuestionerGradeFields.COLUMN_MAX_GRADE + " INTEGER,"
            + SQLFieldsRecords.QuestionerGradeFields.COLUMN_STATUS + " TEXT)";
    public static final String SQL_DELETE_QUESTIONER_GRADE = "DROP TABLE IF EXISTS "+ SQLFieldsRecords.QuestionerGradeFields.TABLE_NAME;

    public QuestionerGradeModel() {
    }

    public QuestionerGradeModel(int _ID, String QST_ID, String QST_GRADE, String MIN_GRADE, String MAX_GRADE, String STATUS) {
        this._ID = _ID;
        this.QST_ID = QST_ID;
        this.QST_GRADE = QST_GRADE;
        this.MIN_GRADE = MIN_GRADE;
        this.MAX_GRADE = MAX_GRADE;
        this.STATUS = STATUS;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getQST_GRADE() {
        return QST_GRADE;
    }

    public void setQST_GRADE(String QST_GRADE) {
        this.QST_GRADE = QST_GRADE;
    }

    public String getMIN_GRADE() {
        return MIN_GRADE;
    }

    public void setMIN_GRADE(String MIN_GRADE) {
        this.MIN_GRADE = MIN_GRADE;
    }

    public String getMAX_GRADE() {
        return MAX_GRADE;
    }

    public void setMAX_GRADE(String MAX_GRADE) {
        this.MAX_GRADE = MAX_GRADE;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
