package com.creativeit.csemobile.model;

/**
 * Created by eko on 29/06/18.
 */

public class QSModel {
    private String L0,L1,L2,L3,L4,L5,L6, L7, L8, L9, L10, L11,
            P_QST_ID,QST_ID,
            QST_LVL,QST_CAPTION,
            QST_NOTE_1,QST_NOTE_2,
            QST_NOTE_3,QST_NOTE_4,
            QST_NOTE_5,QST_LIST_TYPE,
            QST_POINT,LINE_NUM;

    public QSModel() {
    }

    public QSModel(String l0, String l1, String l2, String l3,
                   String l4, String l5, String l6, String l7,
                   String l8, String l9, String l10, String l11,
                   String p_QST_ID, String QST_ID, String QST_LVL,
                   String QST_CAPTION, String QST_NOTE_1, String QST_NOTE_2,
                   String QST_NOTE_3, String QST_NOTE_4, String QST_NOTE_5,
                   String QST_LIST_TYPE, String QST_POINT, String LINE_NUM) {
        L0 = l0;
        L1 = l1;
        L2 = l2;
        L3 = l3;
        L4 = l4;
        L5 = l5;
        L6 = l6;
        L7 = l7;
        L8 = l8;
        L9 = l9;
        L10 = l10;
        L11 = l11;
        P_QST_ID = p_QST_ID;
        this.QST_ID = QST_ID;
        this.QST_LVL = QST_LVL;
        this.QST_CAPTION = QST_CAPTION;
        this.QST_NOTE_1 = QST_NOTE_1;
        this.QST_NOTE_2 = QST_NOTE_2;
        this.QST_NOTE_3 = QST_NOTE_3;
        this.QST_NOTE_4 = QST_NOTE_4;
        this.QST_NOTE_5 = QST_NOTE_5;
        this.QST_LIST_TYPE = QST_LIST_TYPE;
        this.QST_POINT = QST_POINT;
        this.LINE_NUM = LINE_NUM;
    }

    public String getL0() {
        return L0;
    }

    public void setL0(String l0) {
        L0 = l0;
    }

    public String getL1() {
        return L1;
    }

    public void setL1(String l1) {
        L1 = l1;
    }

    public String getL2() {
        return L2;
    }

    public void setL2(String l2) {
        L2 = l2;
    }

    public String getL3() {
        return L3;
    }

    public void setL3(String l3) {
        L3 = l3;
    }

    public String getL4() {
        return L4;
    }

    public void setL4(String l4) {
        L4 = l4;
    }

    public String getL5() {
        return L5;
    }

    public void setL5(String l5) {
        L5 = l5;
    }

    public String getL6() {
        return L6;
    }

    public void setL6(String l6) {
        L6 = l6;
    }

    public String getL7() {
        return L7;
    }

    public void setL7(String l7) {
        L7 = l7;
    }

    public String getL8() {
        return L8;
    }

    public void setL8(String l8) {
        L8 = l8;
    }

    public String getL9() {
        return L9;
    }

    public void setL9(String l9) {
        L9 = l9;
    }

    public String getL10() {
        return L10;
    }

    public void setL10(String l10) {
        L10 = l10;
    }

    public String getL11() {
        return L11;
    }

    public void setL11(String l11) {
        L11 = l11;
    }

    public String getP_QST_ID() {
        return P_QST_ID;
    }

    public void setP_QST_ID(String p_QST_ID) {
        P_QST_ID = p_QST_ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getQST_LVL() {
        return QST_LVL;
    }

    public void setQST_LVL(String QST_LVL) {
        this.QST_LVL = QST_LVL;
    }

    public String getQST_CAPTION() {
        return QST_CAPTION;
    }

    public void setQST_CAPTION(String QST_CAPTION) {
        this.QST_CAPTION = QST_CAPTION;
    }

    public String getQST_NOTE_1() {
        return QST_NOTE_1;
    }

    public void setQST_NOTE_1(String QST_NOTE_1) {
        this.QST_NOTE_1 = QST_NOTE_1;
    }

    public String getQST_NOTE_2() {
        return QST_NOTE_2;
    }

    public void setQST_NOTE_2(String QST_NOTE_2) {
        this.QST_NOTE_2 = QST_NOTE_2;
    }

    public String getQST_NOTE_3() {
        return QST_NOTE_3;
    }

    public void setQST_NOTE_3(String QST_NOTE_3) {
        this.QST_NOTE_3 = QST_NOTE_3;
    }

    public String getQST_NOTE_4() {
        return QST_NOTE_4;
    }

    public void setQST_NOTE_4(String QST_NOTE_4) {
        this.QST_NOTE_4 = QST_NOTE_4;
    }

    public String getQST_NOTE_5() {
        return QST_NOTE_5;
    }

    public void setQST_NOTE_5(String QST_NOTE_5) {
        this.QST_NOTE_5 = QST_NOTE_5;
    }

    public String getQST_LIST_TYPE() {
        return QST_LIST_TYPE;
    }

    public void setQST_LIST_TYPE(String QST_LIST_TYPE) {
        this.QST_LIST_TYPE = QST_LIST_TYPE;
    }

    public String getQST_POINT() {
        return QST_POINT;
    }

    public void setQST_POINT(String QST_POINT) {
        this.QST_POINT = QST_POINT;
    }

    public String getLINE_NUM() {
        return LINE_NUM;
    }

    public void setLINE_NUM(String LINE_NUM) {
        this.LINE_NUM = LINE_NUM;
    }
}
