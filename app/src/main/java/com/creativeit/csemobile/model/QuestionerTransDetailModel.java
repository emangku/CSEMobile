package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerTransDetailModel {
    private int _ID;
    private String QTR_ID, QST_ID, QST_CHECK, COMMENTS,STATUS;

    public static final String SQL_CREATE_QUESTIONER_TRANS_DETAIL = "CREATE TABLE "
            + SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME + " ("
            + SQLFieldsRecords.QuestionerTransDetailFields._ID + "INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QTR_ID + "TEXT,"
            + SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_ID + "TEXT,"
            + SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_QST_CHECK + "TEXT,"
            + SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_COMMENTS + "TEXT,"
            + SQLFieldsRecords.QuestionerTransDetailFields.COLUMN_STATUS + "TEXT)";
    public static final String SQL_DELETE_QUESTIONER_TRANS_DETAIL = "DROP TABLE "+ SQLFieldsRecords.QuestionerTransDetailFields.TABLE_NAME;

    public QuestionerTransDetailModel() {}

    public QuestionerTransDetailModel(int _ID,String QTR_ID, String QST_ID, String QST_CHECK, String COMMENTS,String STATUS) {
        this._ID = _ID;
        this.QTR_ID = QTR_ID;
        this.QST_ID = QST_ID;
        this.QST_CHECK = QST_CHECK;
        this.COMMENTS = COMMENTS;
        this.STATUS = STATUS;
    }
    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getQTR_ID() {
        return QTR_ID;
    }

    public void setQTR_ID(String QTR_ID) {
        this.QTR_ID = QTR_ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getQST_CHECK() {
        return QST_CHECK;
    }

    public void setQST_CHECK(String QST_CHECK) {
        this.QST_CHECK = QST_CHECK;
    }

    public String getCOMMENTS() {
        return COMMENTS;
    }

    public void setCOMMENTS(String COMMENTS) {
        this.COMMENTS = COMMENTS;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
