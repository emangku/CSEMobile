package com.creativeit.csemobile.model;

/**
 * Created by Creef on 6/6/2018.
 */

public class ModelQSTLvL2 {
            private String P_QST_ID, QST_ID,
            QST_LVL, QST_CAPTION,
            QST_NOTE_1, QST_NOTE_2,
            QST_NOTE_3, QST_NOTE_4,
            QST_NOTE_5, QST_LIST_TYPE,
            QST_GRADE_A, MIN_GRADE_A,
            MAX_GRADE_A, QST_GRADE_B,
            MIN_GRADE_B, MAX_GRADE_B,
            QST_GRADE_C, MIN_GRADE_C,
            MAX_GRADE_C, QST_GRADE_D,
            MIN_GRADE_D, MAX_GRADE_D;
    public ModelQSTLvL2(){}
    public ModelQSTLvL2(String p_QST_ID, String QST_ID,
                        String QST_LVL, String QST_CAPTION,
                        String QST_NOTE_1, String QST_NOTE_2,
                        String QST_NOTE_3, String QST_NOTE_4,
                        String QST_NOTE_5, String QST_LIST_TYPE,
                        String QST_GRADE_A, String MIN_GRADE_A,
                        String MAX_GRADE_A, String QST_GRADE_B,
                        String MIN_GRADE_B, String MAX_GRADE_B,
                        String QST_GRADE_C, String MIN_GRADE_C,
                        String MAX_GRADE_C, String QST_GRADE_D,
                        String MIN_GRADE_D, String MAX_GRADE_D) {
        P_QST_ID = p_QST_ID;
        this.QST_ID = QST_ID;
        this.QST_LVL = QST_LVL;
        this.QST_CAPTION = QST_CAPTION;
        this.QST_NOTE_1 = QST_NOTE_1;
        this.QST_NOTE_2 = QST_NOTE_2;
        this.QST_NOTE_3 = QST_NOTE_3;
        this.QST_NOTE_4 = QST_NOTE_4;
        this.QST_NOTE_5 = QST_NOTE_5;
        this.QST_LIST_TYPE = QST_LIST_TYPE;
        this.QST_GRADE_A = QST_GRADE_A;
        this.MIN_GRADE_A = MIN_GRADE_A;
        this.MAX_GRADE_A = MAX_GRADE_A;
        this.QST_GRADE_B = QST_GRADE_B;
        this.MIN_GRADE_B = MIN_GRADE_B;
        this.MAX_GRADE_B = MAX_GRADE_B;
        this.QST_GRADE_C = QST_GRADE_C;
        this.MIN_GRADE_C = MIN_GRADE_C;
        this.MAX_GRADE_C = MAX_GRADE_C;
        this.QST_GRADE_D = QST_GRADE_D;
        this.MIN_GRADE_D = MIN_GRADE_D;
        this.MAX_GRADE_D = MAX_GRADE_D;
    }

    public String getP_QST_ID() {
        return P_QST_ID;
    }

    public void setP_QST_ID(String p_QST_ID) {
        P_QST_ID = p_QST_ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getQST_LVL() {
        return QST_LVL;
    }

    public void setQST_LVL(String QST_LVL) {
        this.QST_LVL = QST_LVL;
    }

    public String getQST_CAPTION() {
        return QST_CAPTION;
    }

    public void setQST_CAPTION(String QST_CAPTION) {
        this.QST_CAPTION = QST_CAPTION;
    }

    public String getQST_NOTE_1() {
        return QST_NOTE_1;
    }

    public void setQST_NOTE_1(String QST_NOTE_1) {
        this.QST_NOTE_1 = QST_NOTE_1;
    }

    public String getQST_NOTE_2() {
        return QST_NOTE_2;
    }

    public void setQST_NOTE_2(String QST_NOTE_2) {
        this.QST_NOTE_2 = QST_NOTE_2;
    }

    public String getQST_NOTE_3() {
        return QST_NOTE_3;
    }

    public void setQST_NOTE_3(String QST_NOTE_3) {
        this.QST_NOTE_3 = QST_NOTE_3;
    }

    public String getQST_NOTE_4() {
        return QST_NOTE_4;
    }

    public void setQST_NOTE_4(String QST_NOTE_4) {
        this.QST_NOTE_4 = QST_NOTE_4;
    }

    public String getQST_NOTE_5() {
        return QST_NOTE_5;
    }

    public void setQST_NOTE_5(String QST_NOTE_5) {
        this.QST_NOTE_5 = QST_NOTE_5;
    }

    public String getQST_LIST_TYPE() {
        return QST_LIST_TYPE;
    }

    public void setQST_LIST_TYPE(String QST_LIST_TYPE) {
        this.QST_LIST_TYPE = QST_LIST_TYPE;
    }

    public String getQST_GRADE_A() {
        return QST_GRADE_A;
    }

    public void setQST_GRADE_A(String QST_GRADE_A) {
        this.QST_GRADE_A = QST_GRADE_A;
    }

    public String getMIN_GRADE_A() {
        return MIN_GRADE_A;
    }

    public void setMIN_GRADE_A(String MIN_GRADE_A) {
        this.MIN_GRADE_A = MIN_GRADE_A;
    }

    public String getMAX_GRADE_A() {
        return MAX_GRADE_A;
    }

    public void setMAX_GRADE_A(String MAX_GRADE_A) {
        this.MAX_GRADE_A = MAX_GRADE_A;
    }

    public String getQST_GRADE_B() {
        return QST_GRADE_B;
    }

    public void setQST_GRADE_B(String QST_GRADE_B) {
        this.QST_GRADE_B = QST_GRADE_B;
    }

    public String getMIN_GRADE_B() {
        return MIN_GRADE_B;
    }

    public void setMIN_GRADE_B(String MIN_GRADE_B) {
        this.MIN_GRADE_B = MIN_GRADE_B;
    }

    public String getMAX_GRADE_B() {
        return MAX_GRADE_B;
    }

    public void setMAX_GRADE_B(String MAX_GRADE_B) {
        this.MAX_GRADE_B = MAX_GRADE_B;
    }

    public String getQST_GRADE_C() {
        return QST_GRADE_C;
    }

    public void setQST_GRADE_C(String QST_GRADE_C) {
        this.QST_GRADE_C = QST_GRADE_C;
    }

    public String getMIN_GRADE_C() {
        return MIN_GRADE_C;
    }

    public void setMIN_GRADE_C(String MIN_GRADE_C) {
        this.MIN_GRADE_C = MIN_GRADE_C;
    }

    public String getMAX_GRADE_C() {
        return MAX_GRADE_C;
    }

    public void setMAX_GRADE_C(String MAX_GRADE_C) {
        this.MAX_GRADE_C = MAX_GRADE_C;
    }

    public String getQST_GRADE_D() {
        return QST_GRADE_D;
    }

    public void setQST_GRADE_D(String QST_GRADE_D) {
        this.QST_GRADE_D = QST_GRADE_D;
    }

    public String getMIN_GRADE_D() {
        return MIN_GRADE_D;
    }

    public void setMIN_GRADE_D(String MIN_GRADE_D) {
        this.MIN_GRADE_D = MIN_GRADE_D;
    }

    public String getMAX_GRADE_D() {
        return MAX_GRADE_D;
    }

    public void setMAX_GRADE_D(String MAX_GRADE_D) {
        this.MAX_GRADE_D = MAX_GRADE_D;
    }
}
