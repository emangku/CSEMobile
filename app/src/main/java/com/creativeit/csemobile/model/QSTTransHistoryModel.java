package com.creativeit.csemobile.model;

/**
 * Created by Creef on 7/6/2018.
 */

public class QSTTransHistoryModel {
    private  String QTR_ID, QST_ID, QST_CAPTION, QTR_STORE_CODE, QTR_STORE_NAME,
            REG_CODE, BAL,QTR_DATE,  QTR_BY,  QTR_USRMS_NAME, AK_BY, AK_BY_NAME,TOTAL, QST_GRADE;

    public QSTTransHistoryModel() {
    }

    public QSTTransHistoryModel(String QTR_ID, String QST_ID, String QST_CAPTION, String QTR_STORE_CODE,
                                String QTR_STORE_NAME, String REG_CODE, String BAL, String QTR_DATE, String QTR_BY, String QTR_USRMS_NAME, String AK_BY, String AK_BY_NAME, String TOTAL, String QST_GRADE) {
        this.QTR_ID = QTR_ID;
        this.QST_ID = QST_ID;
        this.QST_CAPTION = QST_CAPTION;
        this.QTR_STORE_CODE = QTR_STORE_CODE;
        this.QTR_STORE_NAME = QTR_STORE_NAME;
        this.REG_CODE = REG_CODE;
        this.BAL = BAL;
        this.QTR_DATE = QTR_DATE;
        this.QTR_BY = QTR_BY;
        this.QTR_USRMS_NAME = QTR_USRMS_NAME;
        this.AK_BY = AK_BY;
        this.AK_BY_NAME = AK_BY_NAME;
        this.TOTAL = TOTAL;
        this.QST_GRADE = QST_GRADE;
    }

    public String getQTR_ID() {
        return QTR_ID;
    }

    public void setQTR_ID(String QTR_ID) {
        this.QTR_ID = QTR_ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getQST_CAPTION() {
        return QST_CAPTION;
    }

    public void setQST_CAPTION(String QST_CAPTION) {
        this.QST_CAPTION = QST_CAPTION;
    }

    public String getQTR_STORE_CODE() {
        return QTR_STORE_CODE;
    }

    public void setQTR_STORE_CODE(String QTR_STORE_CODE) {
        this.QTR_STORE_CODE = QTR_STORE_CODE;
    }

    public String getQTR_STORE_NAME() {
        return QTR_STORE_NAME;
    }

    public void setQTR_STORE_NAME(String QTR_STORE_NAME) {
        this.QTR_STORE_NAME = QTR_STORE_NAME;
    }

    public String getREG_CODE() {
        return REG_CODE;
    }

    public void setREG_CODE(String REG_CODE) {
        this.REG_CODE = REG_CODE;
    }

    public String getBAL() {
        return BAL;
    }

    public void setBAL(String BAL) {
        this.BAL = BAL;
    }

    public String getQTR_DATE() {
        return QTR_DATE;
    }

    public void setQTR_DATE(String QTR_DATE) {
        this.QTR_DATE = QTR_DATE;
    }

    public String getQTR_BY() {
        return QTR_BY;
    }

    public void setQTR_BY(String QTR_BY) {
        this.QTR_BY = QTR_BY;
    }

    public String getQTR_USRMS_NAME() {
        return QTR_USRMS_NAME;
    }

    public void setQTR_USRMS_NAME(String QTR_USRMS_NAME) {
        this.QTR_USRMS_NAME = QTR_USRMS_NAME;
    }

    public String getAK_BY() {
        return AK_BY;
    }

    public void setAK_BY(String AK_BY) {
        this.AK_BY = AK_BY;
    }

    public String getAK_BY_NAME() {
        return AK_BY_NAME;
    }

    public void setAK_BY_NAME(String AK_BY_NAME) {
        this.AK_BY_NAME = AK_BY_NAME;
    }

    public String getTOTAL() {
        return TOTAL;
    }

    public void setTOTAL(String TOTAL) {
        this.TOTAL = TOTAL;
    }

    public String getQST_GRADE() {
        return QST_GRADE;
    }

    public void setQST_GRADE(String QST_GRADE) {
        this.QST_GRADE = QST_GRADE;
    }
}
