package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class UserAccessModel {
    private int _ID;
    private String USRMS_ID, USRMS_ST,STATUS;
    public static final String SQL_CREATE_USER_ACCESS = "CREATE TABLE "
            + SQLFieldsRecords.UserAccessFields.TABLE_NAME + " ("
            + SQLFieldsRecords.UserAccessFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ID + " INTEGER,"
            + SQLFieldsRecords.UserAccessFields.COLUMN_USRMS_ST + " INTEGER,"
            + SQLFieldsRecords.UserAccessFields.COLUMN_STATUS + " TEXT)";

    public static final String SQL_DELETE_USER_ACCESS = "DROP TABLE IF EXISTS "+ SQLFieldsRecords.UserAccessFields.TABLE_NAME;
    public UserAccessModel() {
    }

    public UserAccessModel(int _ID,String USRMS_ID, String USRMS_ST,String STATUS) {
        this._ID = _ID;
        this.USRMS_ID = USRMS_ID;
        this.USRMS_ST = USRMS_ST;
        this.STATUS = STATUS;
    }
    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getUSRMS_ST() {
        return USRMS_ST;
    }

    public void setUSRMS_ST(String USRMS_ST) {
        this.USRMS_ST = USRMS_ST;
    }

    public String getUSRMS_ID() {
        return USRMS_ID;
    }

    public void setUSRMS_ID(String USRMS_ID) {
        this.USRMS_ID = USRMS_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
