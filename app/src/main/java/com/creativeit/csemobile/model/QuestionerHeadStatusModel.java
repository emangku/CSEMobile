package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerHeadStatusModel {
    private int _ID;
    private String QST_ID, REG_CODE, QST_MONTH, QST_YEAR, QST_STATUS, STATUS_DATE, STATUS_BY,STATUS;

    public static final String SQL_CREATE_QUESTIONER_HEAD_STATUS = "CREATE TABLE "
            + SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME + " ("
            + SQLFieldsRecords.QuestionerHeadStatusFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_REG_CODE + " TEXT,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_MONTH + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_YEAR + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_QST_STATUS + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_DATE + " TEXT,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS_BY + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadStatusFields.COLUMN_STATUS + " TEXT)";
    public static final String SQL_DELETE_QUESTIONER_HEAD_STATUS = "DROP TABLE IF EXISTS "+ SQLFieldsRecords.QuestionerHeadStatusFields.TABLE_NAME;

    public QuestionerHeadStatusModel() {
    }

    public QuestionerHeadStatusModel(int _ID,String QST_ID, String REG_CODE, String QST_MONTH,
                                     String QST_YEAR, String QST_STATUS, String STATUS_DATE, String STATUS_BY, String STATUS) {
        this._ID = _ID;
        this.QST_ID = QST_ID;
        this.REG_CODE = REG_CODE;
        this.QST_MONTH = QST_MONTH;
        this.QST_YEAR = QST_YEAR;
        this.QST_STATUS = QST_STATUS;
        this.STATUS_DATE = STATUS_DATE;
        this.STATUS_BY = STATUS_BY;
        this.STATUS = STATUS;
    }
    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getREG_CODE() {
        return REG_CODE;
    }

    public void setREG_CODE(String REG_CODE) {
        this.REG_CODE = REG_CODE;
    }

    public String getQST_MONTH() {
        return QST_MONTH;
    }

    public void setQST_MONTH(String QST_MONTH) {
        this.QST_MONTH = QST_MONTH;
    }

    public String getQST_YEAR() {
        return QST_YEAR;
    }

    public void setQST_YEAR(String QST_YEAR) {
        this.QST_YEAR = QST_YEAR;
    }

    public String getQST_STATUS() {
        return QST_STATUS;
    }

    public void setQST_STATUS(String QST_STATUS) {
        this.QST_STATUS = QST_STATUS;
    }

    public String getSTATUS_DATE() {
        return STATUS_DATE;
    }

    public void setSTATUS_DATE(String STATUS_DATE) {
        this.STATUS_DATE = STATUS_DATE;
    }

    public String getSTATUS_BY() {
        return STATUS_BY;
    }

    public void setSTATUS_BY(String STATUS_BY) {
        this.STATUS_BY = STATUS_BY;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
