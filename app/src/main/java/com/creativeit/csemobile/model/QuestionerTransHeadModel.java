package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerTransHeadModel {
    private int _ID;
    private String QTR_ID, QST_ID, STORE_CODE, QTR_DATE, QTR_BY,STATUS;

    public static final String SQL_CREATE_QUESTIONER_TRANS_HEADER = "CREATE TABLE "
            + SQLFieldsRecords.QuestionerTransHeaderFields.TABLE_NAME + " ("
            + SQLFieldsRecords.QuestionerTransHeaderFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QST_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STORE_CODE + " TEXT,"
            + SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_DATE + " TEXT,"
            + SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_QTR_BY + " INTEGER,"
            + SQLFieldsRecords.QuestionerTransHeaderFields.COLUMN_STATUS + " TEXT)";

    public static final String SQL_DELETE_QUESTIONER_TRANS_HEADER = "DROP TABLE IF EXISTS "+ SQLFieldsRecords.QuestionerTransHeaderFields.TABLE_NAME;
    public QuestionerTransHeadModel() {
    }

    public QuestionerTransHeadModel(int _ID, String QTR_ID, String QST_ID, String STORE_CODE,
                                    String QTR_DATE, String QTR_BY,String STATUS) {
        this._ID =_ID;
        this.QTR_ID = QTR_ID;
        this.QST_ID = QST_ID;
        this.STORE_CODE = STORE_CODE;
        this.QTR_DATE = QTR_DATE;
        this.QTR_BY = QTR_BY;
        this.STATUS = STATUS;
    }
    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getQTR_ID() {
        return QTR_ID;
    }

    public void setQTR_ID(String QTR_ID) {
        this.QTR_ID = QTR_ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getSTORE_CODE() {
        return STORE_CODE;
    }

    public void setSTORE_CODE(String STORE_CODE) {
        this.STORE_CODE = STORE_CODE;
    }

    public String getQTR_DATE() {
        return QTR_DATE;
    }

    public void setQTR_DATE(String QTR_DATE) {
        this.QTR_DATE = QTR_DATE;
    }

    public String getQTR_BY() {
        return QTR_BY;
    }

    public void setQTR_BY(String QTR_BY) {
        this.QTR_BY = QTR_BY;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
