package com.creativeit.csemobile.model;

/**
 * Created by eko on 07/06/18.
 */

public class ModelLvL33 {

    private String P_QST_LVL3,QST_ID_LVL_3,
            QST_CAPTION_LVL_3,QST_TYPE_LVL_3,
            QST_ID_LVL_4,QST_CAPTION_LVL_4,
            QST_TYPE_LVL_4,QST_POINT_LVL_4;


    public ModelLvL33() {
    }

    public ModelLvL33(String p_QST_LVL3, String QST_ID_LVL_3,
                      String QST_CAPTION_LVL_3, String QST_TYPE_LVL_3,
                      String QST_ID_LVL_4, String QST_CAPTION_LVL_4,
                      String QST_TYPE_LVL_4, String QST_POINT_LVL_4) {
        P_QST_LVL3 = p_QST_LVL3;
        this.QST_ID_LVL_3 = QST_ID_LVL_3;
        this.QST_CAPTION_LVL_3 = QST_CAPTION_LVL_3;
        this.QST_TYPE_LVL_3 = QST_TYPE_LVL_3;
        this.QST_ID_LVL_4 = QST_ID_LVL_4;
        this.QST_CAPTION_LVL_4 = QST_CAPTION_LVL_4;
        this.QST_TYPE_LVL_4 = QST_TYPE_LVL_4;
        this.QST_POINT_LVL_4 = QST_POINT_LVL_4;
    }

    public String getP_QST_LVL3() {
        return P_QST_LVL3;
    }

    public void setP_QST_LVL3(String p_QST_LVL3) {
        P_QST_LVL3 = p_QST_LVL3;
    }

    public String getQST_ID_LVL_3() {
        return QST_ID_LVL_3;
    }

    public void setQST_ID_LVL_3(String QST_ID_LVL_3) {
        this.QST_ID_LVL_3 = QST_ID_LVL_3;
    }

    public String getQST_CAPTION_LVL_3() {
        return QST_CAPTION_LVL_3;
    }

    public void setQST_CAPTION_LVL_3(String QST_CAPTION_LVL_3) {
        this.QST_CAPTION_LVL_3 = QST_CAPTION_LVL_3;
    }

    public String getQST_TYPE_LVL_3() {
        return QST_TYPE_LVL_3;
    }

    public void setQST_TYPE_LVL_3(String QST_TYPE_LVL_3) {
        this.QST_TYPE_LVL_3 = QST_TYPE_LVL_3;
    }

    public String getQST_ID_LVL_4() {
        return QST_ID_LVL_4;
    }

    public void setQST_ID_LVL_4(String QST_ID_LVL_4) {
        this.QST_ID_LVL_4 = QST_ID_LVL_4;
    }

    public String getQST_CAPTION_LVL_4() {
        return QST_CAPTION_LVL_4;
    }

    public void setQST_CAPTION_LVL_4(String QST_CAPTION_LVL_4) {
        this.QST_CAPTION_LVL_4 = QST_CAPTION_LVL_4;
    }

    public String getQST_TYPE_LVL_4() {
        return QST_TYPE_LVL_4;
    }

    public void setQST_TYPE_LVL_4(String QST_TYPE_LVL_4) {
        this.QST_TYPE_LVL_4 = QST_TYPE_LVL_4;
    }

    public String getQST_POINT_LVL_4() {
        return QST_POINT_LVL_4;
    }

    public void setQST_POINT_LVL_4(String QST_POINT_LVL_4) {
        this.QST_POINT_LVL_4 = QST_POINT_LVL_4;
    }

}
