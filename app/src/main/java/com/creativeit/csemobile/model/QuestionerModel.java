package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerModel {
    private int _ID;
    private String P_QST_ID, QST_ID, QST_LVL, QST_CAPTION, QST_NOTE_1,
            QST_NOTE_2, QST_NOTE_3, QST_NOTE_4, QST_NOTE_5, QST_LIST_TYPE, QST_POINT, STATUS;

    public static final String
            SQL_CREATE_QUESTIONER = "CREATE TABLE "
            + SQLFieldsRecords.QuestionerFields.TABLE_NAME + " ("
            + SQLFieldsRecords.QuestionerFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_P_QST_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_LVL + " INTEGER,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_CAPTION + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_1 + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_2 + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_3 + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_4 + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_NOTE_5 + " TEXT,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_LIST_TYPE + " INTEGER,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_QST_POINT + " INTEGER,"
            + SQLFieldsRecords.QuestionerFields.COLUMN_STATUS + " TEXT)";
    public static final String SQL_DELETE_QUESTIONER = "DROP TABLE IF EXISTS "+ SQLFieldsRecords.QuestionerFields.TABLE_NAME;

    public QuestionerModel() {
    }

    public QuestionerModel(int _ID,String p_QST_ID, String QST_ID, String QST_LVL, String QST_CAPTION, String QST_NOTE_1, String QST_NOTE_2,
                           String QST_NOTE_3, String QST_NOTE_4, String QST_NOTE_5, String QST_LIST_TYPE, String QST_POINT, String STATUS) {
        this._ID = _ID;
        P_QST_ID = p_QST_ID;
        this.QST_ID = QST_ID;
        this.QST_LVL = QST_LVL;
        this.QST_CAPTION = QST_CAPTION;
        this.QST_NOTE_1 = QST_NOTE_1;
        this.QST_NOTE_2 = QST_NOTE_2;
        this.QST_NOTE_3 = QST_NOTE_3;
        this.QST_NOTE_4 = QST_NOTE_4;
        this.QST_NOTE_5 = QST_NOTE_5;
        this.QST_LIST_TYPE = QST_LIST_TYPE;
        this.QST_POINT = QST_POINT;
        this.STATUS = STATUS;
    }
    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getP_QST_ID() {
        return P_QST_ID;
    }

    public void setP_QST_ID(String p_QST_ID) {
        P_QST_ID = p_QST_ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getQST_LVL() {
        return QST_LVL;
    }

    public void setQST_LVL(String QST_LVL) {
        this.QST_LVL = QST_LVL;
    }

    public String getQST_CAPTION() {
        return QST_CAPTION;
    }

    public void setQST_CAPTION(String QST_CAPTION) {
        this.QST_CAPTION = QST_CAPTION;
    }

    public String getQST_NOTE_1() {
        return QST_NOTE_1;
    }

    public void setQST_NOTE_1(String QST_NOTE_1) {
        this.QST_NOTE_1 = QST_NOTE_1;
    }

    public String getQST_NOTE_2() {
        return QST_NOTE_2;
    }

    public void setQST_NOTE_2(String QST_NOTE_2) {
        this.QST_NOTE_2 = QST_NOTE_2;
    }

    public String getQST_NOTE_3() {
        return QST_NOTE_3;
    }

    public void setQST_NOTE_3(String QST_NOTE_3) {
        this.QST_NOTE_3 = QST_NOTE_3;
    }

    public String getQST_NOTE_4() {
        return QST_NOTE_4;
    }

    public void setQST_NOTE_4(String QST_NOTE_4) {
        this.QST_NOTE_4 = QST_NOTE_4;
    }

    public String getQST_NOTE_5() {
        return QST_NOTE_5;
    }

    public void setQST_NOTE_5(String QST_NOTE_5) {
        this.QST_NOTE_5 = QST_NOTE_5;
    }

    public String getQST_LIST_TYPE() {
        return QST_LIST_TYPE;
    }

    public void setQST_LIST_TYPE(String QST_LIST_TYPE) {
        this.QST_LIST_TYPE = QST_LIST_TYPE;
    }

    public String getQST_POINT() {
        return QST_POINT;
    }

    public void setQST_POINT(String QST_POINT) {
        this.QST_POINT = QST_POINT;
    }

    public static String getSqlCreateQuestioner() {
        return SQL_CREATE_QUESTIONER;
    }

    public static String getSqlDeleteQuestioner() {
        return SQL_DELETE_QUESTIONER;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
