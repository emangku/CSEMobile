package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerTransPhotoModel {

    private int _ID;
    private String QTR_ID, FILE_ID, FILE_LOC, QTR_BY,STATUS;
    public static final String SQL_CREATE_QUESTIONER_TRANS_PHOTO = "CREATE TABLE "
            + SQLFieldsRecords.QuestionerTransPhotoFields.TABLE_NAME + " ("
            + SQLFieldsRecords.QuestionerTransPhotoFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_ID + " INTEGER,"
            + SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_FILE_LOC + " TEXT,"
            + SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_QTR_BY + " INTEGER,"
            + SQLFieldsRecords.QuestionerTransPhotoFields.COLUMN_STATUS + " TEXT)";

    public static final String SQL_DELETE_QUESTIONER_TRANS_PHOTO = "DROP TABLE IF EXISTS " + SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME;

    public QuestionerTransPhotoModel() {
    }

    public QuestionerTransPhotoModel(int _ID,String QTR_ID, String FILE_ID, String FILE_LOC, String QTR_BY,String STATUS) {
        this._ID = _ID;
        this.QTR_ID = QTR_ID;
        this.FILE_ID = FILE_ID;
        this.FILE_LOC = FILE_LOC;
        this.QTR_BY = QTR_BY;
        this.STATUS = STATUS;
    }
    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getQTR_ID() {
        return QTR_ID;
    }

    public void setQTR_ID(String QTR_ID) {
        this.QTR_ID = QTR_ID;
    }

    public String getFILE_ID() {
        return FILE_ID;
    }

    public void setFILE_ID(String FILE_ID) {
        this.FILE_ID = FILE_ID;
    }

    public String getFILE_LOC() {
        return FILE_LOC;
    }

    public void setFILE_LOC(String FILE_LOC) {
        this.FILE_LOC = FILE_LOC;
    }

    public String getQTR_BY() {
        return QTR_BY;
    }

    public void setQTR_BY(String QTR_BY) {
        this.QTR_BY = QTR_BY;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
