package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerHeadModel {
    private int _ID;
    private String QST_ID, REG_CODE,QST_MONTH,QST_YEAR,QST_STATUS,DATA_DATE,DATA_BY, STATUS;

    public static final String SQL_CREATE_QUESTIONER_HEAD = " CREATE TABLE "
            + SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME +" ("
            + SQLFieldsRecords.QuestionerHeadFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_REG_CODE + " TEXT,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_MONTH + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_YEAR + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_QST_STATUS + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_DATE + " TEXT,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_DATA_BY + " INTEGER,"
            + SQLFieldsRecords.QuestionerHeadFields.COLUMN_STATUS + " TEXT)";
    public static final String SQL_DELETE_QUESTIONER_HEAD = "DROP TABLE IF EXISTS "+ SQLFieldsRecords.QuestionerHeadFields.TABLE_NAME;

    public QuestionerHeadModel() {}

    public QuestionerHeadModel(int _ID,String QST_ID, String REG_CODE, String QST_MONTH,
                               String QST_YEAR, String QST_STATUS, String DATA_DATE, String DATA_BY, String STATUS) {
        this._ID = _ID;
        this.QST_ID = QST_ID;
        this.REG_CODE = REG_CODE;
        this.QST_MONTH = QST_MONTH;
        this.QST_YEAR = QST_YEAR;
        this.QST_STATUS = QST_STATUS;
        this.DATA_DATE = DATA_DATE;
        this.DATA_BY = DATA_BY;
        this.STATUS = STATUS;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getREG_CODE() {
        return REG_CODE;
    }

    public void setREG_CODE(String REG_CODE) {
        this.REG_CODE = REG_CODE;
    }

    public String getQST_MONTH() {
        return QST_MONTH;
    }

    public void setQST_MONTH(String QST_MONTH) {
        this.QST_MONTH = QST_MONTH;
    }

    public String getQST_YEAR() {
        return QST_YEAR;
    }

    public void setQST_YEAR(String QST_YEAR) {
        this.QST_YEAR = QST_YEAR;
    }

    public String getQST_STATUS() {
        return QST_STATUS;
    }

    public void setQST_STATUS(String QST_STATUS) {
        this.QST_STATUS = QST_STATUS;
    }

    public String getDATA_DATE() {
        return DATA_DATE;
    }

    public void setDATA_DATE(String DATA_DATE) {
        this.DATA_DATE = DATA_DATE;
    }

    public String getDATA_BY() {
        return DATA_BY;
    }

    public void setDATA_BY(String DATA_BY) {
        this.DATA_BY = DATA_BY;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
