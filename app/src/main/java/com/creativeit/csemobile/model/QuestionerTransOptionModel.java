package com.creativeit.csemobile.model;

import com.creativeit.csemobile.helpers.sql_db.SQLFieldsRecords;

/**
 * Created by Creef on 4/26/2018.
 */

public class QuestionerTransOptionModel {
    private int _ID;

    private String QTR_ID, OPT_ID, OPT_CAPTION_1, OPT_CAPTION_2, IS_CHECK,STATUS;

    public static final String SQL_CREATE_QUESTIONER_TRANS_OPTIONS = "CREATE TABLE "
            + SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME + " ("
            + SQLFieldsRecords.QuestionerTransOptionFields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_QTR_ID + " TEXT,"
            + SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_ID + " INTEGER,"
            + SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_1 + " TEXT,"
            + SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_OPT_CAPTION_2 + " TEXT,"
            + SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_IS_CHECK + " INTEGER,"
            + SQLFieldsRecords.QuestionerTransOptionFields.COLUMN_STATUS + " TEXT)";

    public static final String SQL_DELETE_QUESTIONER_TRANS_OPTIONS = "DROP TABLE IF EXISTS " + SQLFieldsRecords.QuestionerTransOptionFields.TABLE_NAME;

    public QuestionerTransOptionModel() {
    }

    public QuestionerTransOptionModel(int _ID,String QTR_ID, String OPT_ID,
                                      String OPT_CAPTION_1, String OPT_CAPTION_2, String IS_CHECK,String STATUS) {
        this._ID = _ID;
        this.QTR_ID = QTR_ID;
        this.OPT_ID = OPT_ID;
        this.OPT_CAPTION_1 = OPT_CAPTION_1;
        this.OPT_CAPTION_2 = OPT_CAPTION_2;
        this.IS_CHECK = IS_CHECK;
        this.STATUS = STATUS;
    }
    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getQTR_ID() {
        return QTR_ID;
    }

    public void setQTR_ID(String QTR_ID) {
        this.QTR_ID = QTR_ID;
    }

    public String getOPT_ID() {
        return OPT_ID;
    }

    public void setOPT_ID(String OPT_ID) {
        this.OPT_ID = OPT_ID;
    }

    public String getOPT_CAPTION_1() {
        return OPT_CAPTION_1;
    }

    public void setOPT_CAPTION_1(String OPT_CAPTION_1) {
        this.OPT_CAPTION_1 = OPT_CAPTION_1;
    }

    public String getOPT_CAPTION_2() {
        return OPT_CAPTION_2;
    }

    public void setOPT_CAPTION_2(String OPT_CAPTION_2) {
        this.OPT_CAPTION_2 = OPT_CAPTION_2;
    }

    public String getIS_CHECK() {
        return IS_CHECK;
    }

    public void setIS_CHECK(String IS_CHECK) {
        this.IS_CHECK = IS_CHECK;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
