package com.creativeit.csemobile.model;

/**
 * Created by Creef on 6/29/2018.
 */

public class DemoModel {
    private String P_QST_ID,QST_ID, QST_LVL,QST_CAPTION, QST_NOTE_1, QST_NOTE_2,QST_LIST_TYPE,QST_POINT;

    public DemoModel() {
    }

    public DemoModel(String p_QST_ID, String QST_ID, String QST_LVL, String QST_CAPTION,
                     String QST_NOTE_1, String QST_NOTE_2, String QST_LIST_TYPE, String QST_POINT) {
        P_QST_ID = p_QST_ID;
        this.QST_ID = QST_ID;
        this.QST_LVL = QST_LVL;
        this.QST_CAPTION = QST_CAPTION;
        this.QST_NOTE_1 = QST_NOTE_1;
        this.QST_NOTE_2 = QST_NOTE_2;
        this.QST_LIST_TYPE = QST_LIST_TYPE;
        this.QST_POINT = QST_POINT;
    }

    public String getP_QST_ID() {
        return P_QST_ID;
    }

    public void setP_QST_ID(String p_QST_ID) {
        P_QST_ID = p_QST_ID;
    }

    public String getQST_ID() {
        return QST_ID;
    }

    public void setQST_ID(String QST_ID) {
        this.QST_ID = QST_ID;
    }

    public String getQST_LVL() {
        return QST_LVL;
    }

    public void setQST_LVL(String QST_LVL) {
        this.QST_LVL = QST_LVL;
    }

    public String getQST_CAPTION() {
        return QST_CAPTION;
    }

    public void setQST_CAPTION(String QST_CAPTION) {
        this.QST_CAPTION = QST_CAPTION;
    }

    public String getQST_NOTE_1() {
        return QST_NOTE_1;
    }

    public void setQST_NOTE_1(String QST_NOTE_1) {
        this.QST_NOTE_1 = QST_NOTE_1;
    }

    public String getQST_NOTE_2() {
        return QST_NOTE_2;
    }

    public void setQST_NOTE_2(String QST_NOTE_2) {
        this.QST_NOTE_2 = QST_NOTE_2;
    }

    public String getQST_LIST_TYPE() {
        return QST_LIST_TYPE;
    }

    public void setQST_LIST_TYPE(String QST_LIST_TYPE) {
        this.QST_LIST_TYPE = QST_LIST_TYPE;
    }

    public String getQST_POINT() {
        return QST_POINT;
    }

    public void setQST_POINT(String QST_POINT) {
        this.QST_POINT = QST_POINT;
    }
}
