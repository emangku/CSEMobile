package com.creativeit.csemobile.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.activity.DetailTransActivity;
import com.creativeit.csemobile.helpers.utils;
import com.creativeit.csemobile.model.QSTTransHistoryModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Creef on 7/6/2018.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private View view;
    private Context context;
    private List<QSTTransHistoryModel> list = new ArrayList<>();

    public HistoryAdapter(Context context, List<QSTTransHistoryModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_history,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder holder, final int position) {
        holder.viewQuestioner.setText(list.get(position).getQST_ID() +" - "+list.get(position).getQTR_ID());
        holder.viewDateTime.setText(list.get(position).getQTR_DATE());
        holder.viewStore.setText(list.get(position).getQTR_STORE_CODE()+" - "+ list.get(position).getQTR_STORE_NAME());
        holder.viewOnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.setDataSession(view.getContext(),"sessionQTR_STORE_CODE",list.get(position).getQTR_STORE_CODE());
                view.getContext().startActivity(new Intent(view.getContext(), DetailTransActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null){
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.viewQuestioner) TextView viewQuestioner;
        @BindView(R.id.viewDateTime) TextView viewDateTime;
        @BindView(R.id.viewStore) TextView viewStore;
        @BindView(R.id.viewGrandPoint) TextView viewGrandPoint;
        @BindView(R.id.viewRank) TextView viewRank;
        @BindView(R.id.viewOnClick)LinearLayout viewOnClick;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
