package com.creativeit.csemobile.adapter.sync;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.model.QuestionerModel;

import java.util.List;

/**
 * Created by Creef on 5/14/2018.
 */

public class QSTAdapter extends ArrayAdapter<QuestionerModel> {
    private List<QuestionerModel> list;
    private Context context;

    public QSTAdapter(Context context, int resource, List<QuestionerModel> mList) {
        super(context, resource, mList);
        this.context = context;
        this.list = mList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View views = inflater.inflate(R.layout.questioner_list, null);
        TextView viewName = (TextView) views.findViewById(R.id.viewName);
        ImageView img = (ImageView) views.findViewById(R.id.viewIcon);
        QuestionerModel model = list.get(position);


        if (model.getSTATUS() == "0")
            img.setBackgroundResource(R.drawable.isoffline);
        else
            img.setBackgroundResource(R.drawable.issuccess);

        return views;
    }
}
