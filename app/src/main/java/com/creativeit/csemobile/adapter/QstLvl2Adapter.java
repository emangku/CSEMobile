package com.creativeit.csemobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.model.ModelQstAll;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Creef on 6/6/2018.
 */

public class QstLvl2Adapter extends RecyclerView.Adapter<QstLvl2Adapter.ViewHolder> {
    private Context context;
    private View view;
    private List<ModelQstAll> list = new ArrayList<>();

    public QstLvl2Adapter(Context context, List<ModelQstAll> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public QstLvl2Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_qst, null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QstLvl2Adapter.ViewHolder holder, int position) {
        holder.viewLevel1.setText(list.get(position).getQST_CAPTION());
        holder.viewTlt1.setText(list.get(position).getQST_NOTE_1());
        holder.viewBy1.setText(list.get(position).getQST_NOTE_2());
        holder.viewgradeA.setText(list.get(position).getQST_GRADE_A()+" : "+list.get(position).getMIN_GRADE_A()+" - "+list.get(position).getMAX_GRADE_A());
        holder.viewgradeB.setText(list.get(position).getQST_GRADE_B()+" : "+list.get(position).getMIN_GRADE_B()+" - "+list.get(position).getMAX_GRADE_B());
        holder.viewgradeC.setText(list.get(position).getQST_GRADE_C()+" : "+list.get(position).getMIN_GRADE_C()+" - "+list.get(position).getMAX_GRADE_C());
    }

    @Override
    public int getItemCount() {
        if (list !=null){
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.viewLevel1)TextView viewLevel1;
        @BindView(R.id.viewTlt1)TextView viewTlt1;
        @BindView(R.id.viewBy1)TextView viewBy1;
        @BindView(R.id.viewgradeA)TextView viewgradeA;
        @BindView(R.id.viewgradeB)TextView viewgradeB;
        @BindView(R.id.viewgradeC)TextView viewgradeC;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
