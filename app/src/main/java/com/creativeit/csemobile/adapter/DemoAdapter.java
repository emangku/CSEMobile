package com.creativeit.csemobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.model.DemoModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Creef on 6/29/2018.
 */

public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.ViewHolder> {
    private View view;
    private Context context;
    private List<DemoModel> list = new ArrayList<>();

    public DemoAdapter(Context context, List<DemoModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public DemoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_demo,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DemoAdapter.ViewHolder holder, int position) {
        holder.viewIdTest.setText(list.get(position).getQST_CAPTION());
    }

    @Override
    public int getItemCount() {
        if (list !=null){
            list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.viewIdTest)TextView viewIdTest;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,view);
        }
    }
}
