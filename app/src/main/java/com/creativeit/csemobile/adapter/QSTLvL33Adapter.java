package com.creativeit.csemobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.creativeit.csemobile.R;
import com.creativeit.csemobile.model.ModelLvL33;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Creef on 6/25/2018.
 */

public class QSTLvL33Adapter extends RecyclerView.Adapter<QSTLvL33Adapter.ViewHolder>{
    private View view;
    private Context context;
    private List<ModelLvL33> list = new ArrayList<>();

    public QSTLvL33Adapter(Context context, List<ModelLvL33> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public QSTLvL33Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_lvl_3, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QSTLvL33Adapter.ViewHolder holder, int position) {
        holder.viewOPT_Qst.setText(list.get(position).getQST_CAPTION_LVL_3());
    }

    @Override
    public int getItemCount() {
        if (list != null){
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.viewOPT_Qst)TextView viewOPT_Qst;
        @BindView(R.id.viewROpt)RadioGroup viewROpt;
        @BindView(R.id.viewYes)RadioButton viewYes;
        @BindView(R.id.viewNo)RadioButton viewNo;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(context, view);
        }
    }
}
